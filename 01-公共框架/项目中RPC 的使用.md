
#### 对外提供的接口必须放到api中

#### 对外提供的detail,vo 类必须放到api 包中否则客户引用不到

##### 包路径

##### 接口放到

项目名.rpc.service

##### 服务类放到项目的

项目名.rpc.service

##### detail 放到

项目名.rpc.detail

##### vo 放到

项目名.rpc.vo

##### 服务注册


```java
package com.hhwy.nettyrpc.test.server;
import java.util.ArrayList;
import java.util.List;
import com.hhwy.demo.api.bean.User;
import com.hhwy.nettyrpc.server.RpcService;

/**
    * 服务注册需要添加注解 @RpcService(接口名)
    * @author jlx
    *
    */
@RpcService(com.hhwy.demo.api.Test.class)
public class TestService implements com.hhwy.demo.api.Test{
/**
    * 接口的实现代码
    */
}
```


#### 客户端调用

``` java

Test test=RemoteProcedureCall.getInstancce().getService(Test.class); 



```