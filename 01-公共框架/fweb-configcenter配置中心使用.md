## 配置中心

### 简介

fweb-platform-configcenter是应用系统的配置中心，所有开启配置中心的应用，配置都可以从配置中心获取，
配置中心的配置分为


#### 配置分类
* 集群配置：集群配置为某一组业务的配置，如  marketing 业务部署了5个节点作为集群，在配置中心创建一个集群配置，把相关配置添加进去即可。
* 实例配置：如果集群中的实例有差异化配置，如序列生成器的id 和data center id, 则可以在每个实例的配置中增加，集群中所有实例的这些配置都是不同的。

#### 应用程序配置获取优先级

* 优先获取环境变量
* 如果环境变量中不存在则从配置中心获取
* 如果配置中心配置不存在则从配置文件中获取
* 需要注意的是，因为配置中心需要连接zookeeper, 所以以下配置必须在环境变量或配置文件中配置，并且不会从配置中心获取这几个的配置值
```
#应用程序所属域, 对应配置中心的域列表的值
common.domain=test
#应用程序集群标识，对应配置中心集群列表的值
common.cluster=fweb-security-server
#应用程序实例标识，对应配置中心实例列表的值
common.instance=fweb-security-server-01
#应用程序zk连接地址，配置中心地址
common.zookeeper.hosts=192.168.14.8:2181
#应用程序配置中心是否开启，只有开启配置中心才会使用配置中心的配置，否则使用环境变量和配置文件中的配置
common.zookeeper.configcenter.open=true
```

### 使用方法

#### 依赖引用

```
fweb-propery 版本必须为0.0.6 以上
<dependency>
    <groupId>com.hhwy</groupId>
    <artifactId>fweb-property</artifactId>
    <version>0.0.6</version>
</dependency>
fweb-property-zookeeper 版本必须为0.0.2 以上
<dependency>
    <groupId>com.hhwy</groupId>
    <artifactId>fweb-property-zookeeper</artifactId>
    <version>0.0.2</version>
</dependency>
```
#### 在配置中心增加配置

1. 在对应域下创建好集群并增加配置（可以先启动一次应用，应用会自动创建好对应的集群和实例，之后再添加配置）
2. 在对应的集群下创建实例，并增加对应的实例配置（可以先启动一次应用，应用会自动创建好对应的集群和实例，之后再添加配置）

#### 在环境变量或配置文件中配置配置中心的相关配置 (在common-config.properties，或环境变量中配置，生产环境要在环境变量中配置，避免发布版本频繁修改文件)

```
#应用程序所属域, 对应配置中心的域列表的值
common.domain=test
#应用程序集群标识，对应配置中心集群列表的值
common.cluster=fweb-security-server
#应用程序实例标识，对应配置中心实例列表的值
common.instance=fweb-security-server-01
#应用程序zk连接地址，配置中心地址
common.zookeeper.hosts=192.168.14.8:2181
#应用程序配置中心是否开启，只有开启配置中心才会使用配置中心的配置，否则使用环境变量和配置文件中的配置
common.zookeeper.configcenter.open=true
```

### 异常处理

如果出现启动失败请检查，fweb-property 0.0.6 ，fweb-property-zookeeper 0.0.2，netty  版本是否有其他低版本，必须为4.1.26 all版本，把其他都去掉

### 主要功能

* 新增集群：
* 编辑集群：
* 删除集群：
* 新增集群配置：
* 编辑集群配置：
* 导入集群配置：导入为properties 文件
* 导出集群配置：导出为properties 文件
* 应用集群配置：将配置同步到应用程序


* 新增实例：
* 编辑实例：
* 删除实例：
* 新增实例配置：
* 编辑实例配置：
* 导入实例配置：导入为properties 文件
* 导出实例配置：导出为properties 文件
* 应用实例配置：将配置同步到应用程序