## 基于Rest的RPC 框架

### 简介

fweb-rest 恒华分布式服务框架，为服务之间提供高性能和透明化的RPC远程服务调用方案。底层使用rest 作为通信基础。和spring 无缝集成。


### 架构图

![image](https://git.365power.cn:8443/jialx/documents/raw/master/images/fweb-rest.png)


### 主要功能

* 服务提供者向注册中心注册提供的接口列表和调用地址

* 客户端维护可用【接口】=【服务地址列表】并与注册中心时时同步

* 注册中心自动发现新服务，并与每个客户端同步

* 注册中心自动发现掉线服务，并与每个客户端同步及时剔除不可用服务

### 管理界面 

* 可以查看服务列表
* 可以查看节点列表并测试

### 工程介绍

#### 项目工程

1. fweb-rest  父工程
1. fweb-rest-admin 管理界面，用来查看注册中心可用服务列表
1. fweb-rest-api 客户端和服务端通信依赖，包括协议，工具类，其他通用配置
1. fweb-rest-client 客户端工程，如果需要访问服务端则只需要加这个依赖
2. fweb-rest-service 服务端工程，如果需要发布服务则只需要加这个依赖

#### 测试工程


1. fweb-rest-demo-api 客户端/服务端通信的约定接口
1. fweb-rest-client-demo 客户端demo
1. fweb-rest-service-demo 服务端demo01.提供 一个接口服务

## 使用配置

### 接口定义
```java
package com.hhwy.demo.api;

import java.util.List;

import com.hhwy.demo.api.bean.User;

public interface Test {

    public String hello(String name);
    
    public User getUser();
	
    public List<User> getUserList();
}
```
### 服务端配置

#### 依赖配置
```xml
<dependency>
		<groupId>com.hhwy</groupId>
		<artifactId>fweb-rest-service</artifactId>
		<version>0.1.9</version>
</dependency>
```

##### 添加配置文件到 class: 或 class:/config     

`名称为：common-config.properties`
#    
    配置文件详解在下边

##### 服务注册
```java
package com.hhwy.nettyrpc.test.server;
import java.util.ArrayList;
import java.util.List;
import com.hhwy.demo.api.bean.User;
import com.hhwy.nettyrpc.server.RpcService;

/**
    * 服务注册需要添加注解 @RpcService(接口名)
    * @author jlx
    *
    */
@RestService(com.hhwy.demo.api.Test.class)
public class TestService implements com.hhwy.demo.api.Test{
/**
    * 接口的实现代码
    */
}
```

### 客户端调用

#### 依赖配置
```xml
<dependency>
        <groupId>com.hhwy</groupId>
        <artifactId>fweb-rest-client</artifactId>
        <version>0.1.9</version>
</dependency>
```

##### 添加配置文件到 class: 或 class:/config     

`名称为：common-config.properties`
#    
    配置文件详解在下边


##### 服务调用
```java
@RequestMapping(value = "/test.do", method = RequestMethod.GET, produces = { "application/json" })
@ResponseBody
public Object testList() {
    
    Map<String, Object> map=new HashMap<>();
    
    Test test= RestClientFactory.getInst().create(Test.class);\\创建接口代理
    
    map.put("test01 result", test.hello("test01")); \\方法调用
    
    Test02 test02= RestClientFactory.getInst().create(Test02.class);
    
    map.put("test02 result",test02.test02("test02"));
    
    System.out.println("---+++--+-+-+-");
    
    return map;
}
```

#### 配置文件 common-config.properties 配置说明

```bash
#zooleper域,客户端与服务端有相同的域才可以调通
common.domain=default
#注册中心地址
common.zookeeper.hosts=192.168.14.101:2181
#调用超时时间
fweb.rpc.invoke.timeout=15000
#rest服务端开启
fweb.rpc.service.enable=true
#rest客户端开启
fweb.rpc.client.enable=true
#rest客户端调式指定机器时使用
fweb.rpc.test.invoke=http://192.168.14.100:8080/collection-platform-web
#服务注册调用地址
fweb.rpc.service.address=http://192.168.14.100:8080/monitor-platform-web
```
