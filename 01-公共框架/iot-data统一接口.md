## iot-data 接口调用

### 请求地址
```
http://10.0.1.94:9090/iot-data-service/iotDataService/
```

### 请求头

* Content-Type ： application/json
* accessToken : 分配的客户端标识获取的token

### 请求路径及参数

#### selectOneDevice 查询单设备数据
#####  完整路径 http://10.0.1.94:9090/iot-data-service/iotDataService/selectOneDevice

##### 请求体
```
{
	"device": 6,//设备ID
	"selectStatement": {
		"tableName": "em_real_data_limit", //查询类型
		"start": "2019-12-05", //数据开始时间
		"end": "2019-12-20",//数据结束时间
		"columns": [{ //查询数据列
				"column": "data_item"

			},
			{
				"column": "max_value"
			},
			{
				"column": "max_value_time"
			},
			{
				"column": "min_value"
			},
			{
				"column": "min_value_time"
			},
			{
				"column": "avg_value"
			}
		],
		"groupbys": []
	}
}
```
##### 返回结果

```
{
"serviceKey": null,
"clientKey": null,
"value":[{
	"ymd": 20200120,
	"timeinterval_03": 23,
	"dataTime": 1579449600000,
	"timeinterval_01": 35,
	"id": "16:2020:01:20:000000",
	"timeinterval_02": 20,
	"time": 0,
	"device": 16,
	"tableName": null
	},
	{
	"ymd": 20200121,
	"timeinterval_03": 24,
	"dataTime": 1579536000000,
	"timeinterval_01": 22,
	"id": "16:2020:01:21:000000",
	"timeinterval_02": 25,
	"time": 0,
	"device": 16,
	"tableName": null
	}],
"state": 200,
"message": "查询成功",
"exception": null
}
```

#### selectOneDeviceFilter 查询单设备数据，带过滤器
#####  完整路径 http://10.0.1.94:9090/iot-data-service/iotDataService/selectOneDeviceFilter

##### 请求体
```
{
	"device": 6,
	"selectStatement": {
		"tableName": "em_real_data_limit",
		"start": "2019-12-05",
		"end": "2019-12-20",
		"columns": [{
				"column": "data_item"

			},
			{
				"column": "max_value"
			},
			{
				"column": "max_value_time"
			},
			{
				"column": "min_value"
			},
			{
				"column": "min_value_time"
			},
			{
				"column": "avg_value"
			}
		],
		"groupbys": []
	},
  "andFilter":[{"columnName":"min_value","columnValue":"0","compareOperator":"EQUAL"}]
}
```
##### 返回结果

```
{
"serviceKey": null,
"clientKey": null,
"value":[{
	"ymd": 20200120,
	"timeinterval_03": 23,
	"dataTime": 1579449600000,
	"timeinterval_01": 35,
	"id": "16:2020:01:20:000000",
	"timeinterval_02": 20,
	"time": 0,
	"device": 16,
	"tableName": null
	},
	{
	"ymd": 20200121,
	"timeinterval_03": 24,
	"dataTime": 1579536000000,
	"timeinterval_01": 22,
	"id": "16:2020:01:21:000000",
	"timeinterval_02": 25,
	"time": 0,
	"device": 16,
	"tableName": null
	}],
"state": 200,
"message": "查询成功",
"exception": null
}
```


#### selectManyDevice 查询多设备数据
#####  完整路径 http://10.0.1.94:9090/iot-data-service/iotDataService/selectManyDevice

##### 请求体
```
{
	"devices": [15,16],
	"selectStatement": {
		"tableName": "em_business_data_collect",
		"start": "2020-02-03",
		"end": "2020-02-05",
		"columns": [{
				"column": "timeinterval_01",
				"aggregationType": "sum"
			},
			{
				"column": "timeinterval_02",
				"aggregationType": "sum"
			},
			{
				"column": "timeinterval_03",
				"aggregationType": "sum"
			}
		],
		"groupbys": []
	}
}
```

##### 返回结果

```
{
"serviceKey": null,
"clientKey": null,
"value":[{
	"ymd": 20200120,
	"timeinterval_03": 23,
	"dataTime": 1579449600000,
	"timeinterval_01": 35,
	"id": "16:2020:01:20:000000",
	"timeinterval_02": 20,
	"time": 0,
	"device": 16,
	"tableName": null
	},
	{
	"ymd": 20200121,
	"timeinterval_03": 24,
	"dataTime": 1579536000000,
	"timeinterval_01": 22,
	"id": "16:2020:01:21:000000",
	"timeinterval_02": 25,
	"time": 0,
	"device": 16,
	"tableName": null
	}],
"state": 200,
"message": "查询成功",
"exception": null
}
```


#### selectManyDeviceFilter 查询多设备数据
#####  完整路径 http://10.0.1.94:9090/iot-data-service/iotDataService/selectManyDeviceFilter

##### 请求体
```
{
	"devices": [15,16],
	"selectStatement": {
		"tableName": "em_business_data_collect",
		"start": "2020-02-03",
		"end": "2020-02-05",
		"columns": [{
				"column": "timeinterval_01",
				"aggregationType": "sum"
			},
			{
				"column": "timeinterval_02",
				"aggregationType": "sum"
			},
			{
				"column": "timeinterval_03",
				"aggregationType": "sum"
			}
		],
		"groupbys": []
	},
	 "andFilter":[{"columnName":"timeinterval_01","columnValue":"100","compareOperator":"EQUAL"}]
}
```

##### 返回结果

```
{
"serviceKey": null,
"clientKey": null,
"value":[{
	"ymd": 20200120,
	"timeinterval_03": 23,
	"dataTime": 1579449600000,
	"timeinterval_01": 35,
	"id": "16:2020:01:20:000000",
	"timeinterval_02": 20,
	"time": 0,
	"device": 16,
	"tableName": null
	},
	{
	"ymd": 20200121,
	"timeinterval_03": 24,
	"dataTime": 1579536000000,
	"timeinterval_01": 22,
	"id": "16:2020:01:21:000000",
	"timeinterval_02": 25,
	"time": 0,
	"device": 16,
	"tableName": null
	}],
"state": 200,
"message": "查询成功",
"exception": null
}
```