## 基于Netty的RPC 框架

### 简介

fweb-rpc 恒华分布式服务框架，为服务之间提供高性能和透明化的RPC远程服务调用方案。底层使用netty nio 作为通信基础。和spring 无缝集成。


### 架构图

![image](https://git.365power.cn/jialx/documents/raw/master/images/rpc-design-img.png)


### 主要功能

* 服务提供者向注册中心注册提供的接口列表和调用地址

* 客户端维护可用【接口】=【服务地址列表】并与注册中心时时同步

* 注册中心自动发现新服务，并与每个客户端同步

* 注册中心自动发现掉线服务，并与每个客户端同步及时剔除不可用服务

* 服务调用，如果不通则路由到下一个服务节点，最多尝试3次。

* 如果同意接口有多个提供者，会在此列表上进行负载均衡

* 支持ip 端口转发的服务发布，只需要在配置文件中指定rpc.service.ip  和 rpc.service.port 就可以了。

* 配置和使用简单

### 管理界面 

* 可以查看服务列表
* 可以查看节点列表并测试

### 工程介绍

#### 项目工程

1. fweb-rpc  父工程
1. fweb-rpc-admin 管理界面，用来查看注册中心可用服务列表
1. fweb-rpc-api 客户端和服务端通信依赖，包括协议，工具类，其他通用配置
1. fweb-rpc-client 客户端工程，如果需要访问服务端则只需要加这个依赖
2. fweb-rpc-service 服务端工程，如果需要发布服务则只需要加这个依赖

#### 测试工程


1. fweb-rpc-demo-api 客户端/服务端通信的约定接口
1. fweb-rpc-client-demo 客户端demo
1. fweb-rpc-service-demo 服务端demo01.提供 一个接口服务

## 使用配置

### 接口定义
```java
package com.hhwy.demo.api;

import java.util.List;

import com.hhwy.demo.api.bean.User;

public interface Test {

    public String hello(String name);
    
    public User getUser();
	
    public List<User> getUserList();
}
```
### 服务端配置

#### 依赖配置
```xml
<dependency>
		<groupId>com.hhwy</groupId>
		<artifactId>fweb-rpc-api</artifactId>
		<version>0.0.1</version>
</dependency>

<dependency>
		<groupId>com.hhwy</groupId>
		<artifactId>fweb-rpc-service</artifactId>
		<version>0.0.1</version>
</dependency>
```

##### spring 服务扫描的包路径 配置注册中心地址 服务管理器 

` <context:component-scan base-package="com.hhwy.nettyrpc.test.server"/>`

##### 添加配置文件到 class: 或 class:/config     

`名称为：netty-rpc-config.properties`
#    
    配置文件详解在下边

##### 服务注册
```java
package com.hhwy.nettyrpc.test.server;
import java.util.ArrayList;
import java.util.List;
import com.hhwy.demo.api.bean.User;
import com.hhwy.nettyrpc.server.RpcService;

/**
    * 服务注册需要添加注解 @RpcService(接口名)
    * @author jlx
    *
    */
@RpcService(com.hhwy.demo.api.Test.class)
public class TestService implements com.hhwy.demo.api.Test{
/**
    * 接口的实现代码
    */
}
```

### 客户端调用

#### 依赖配置
```xml
<dependency>
        <groupId>com.hhwy</groupId>
        <artifactId>fweb-rpc-api</artifactId>
        <version>0.0.1</version>
</dependency>

<dependency>
        <groupId>com.hhwy</groupId>
        <artifactId>fweb-rpc-client</artifactId>
        <version>0.0.1</version>
</dependency>
```

##### 添加配置文件到 class: 或 class:/config     

`名称为：netty-rpc-config.properties`
#    
    配置文件详解在下边


##### 服务调用
```java
@RequestMapping(value = "/test.do", method = RequestMethod.GET, produces = { "application/json" })
@ResponseBody
public Object testList() {
    
    Map<String, Object> map=new HashMap<>();
    
    Test test= RpcClientFactory.getInst().create(Test.class);\\创建接口代理
    
    map.put("test01 result", test.hello("test01")); \\方法调用
    
    Test02 test02= RpcClientFactory.getInst().create(Test02.class);
    
    map.put("test02 result",test02.test02("test02"));
    
    System.out.println("---+++--+-+-+-");
    
    return map;
}
```

#### 配置文件 netty-rpc-config.properties 配置说明

```bash
#注册中心地址
rpc.zk.address=10.0.2.20:2181,10.0.2.20:2182,10.0.2.20:2183
#调用超时时间
rpc.invoke.timeout=15000
#服务注册的IP（要确保此地址客户端可调用）， 默认和绑定的IP 一致
rpc.service.ip=123.57.37.117
#服务注册的端口（要确保此地址客户端可调用）， 默认和绑定的端口 一致
rpc.service.port=8866
#是否开启服务端（默认为只要引用服务端jar就开启）
rpc.service.enable=true
#是否开启客户端 （默认为只要引用客户端jar就开启）
rpc.client.enable=true
```

#### 支持docker 容器内部部署服务

```bash  
#服务注册的IP（要确保此地址客户端可调用）， 默认和绑定的IP 一致
rpc.service.ip=123.57.37.117
#服务注册的端口（要确保此地址客户端可调用）， 默认和绑定的端口 一致
rpc.service.port=8866
```
```
   以上两个配置可以自动根据环境变量改变
   当环境变量中存在 RPC_IP=192.168.5.6
   相当于 rpc.service.ip=192.168.5.6
```

```
   当环境变量中存在 RPC_PORT=8066
   相当于 rpc.service.port=8066
```
####  配置优先级

##### IP
```
当配置文件中配置 rpc.service.ip=10.0.2.20 
并且环境变量中存在 RPC_IP=192.168.5.6
则真正的服务IP=10.0.2.20
也就是配置文件中的优先级大于环境变量
```
##### 端口
```
当配置文件中配置 rpc.service.port=8066
并且环境变量中存在 RPC_IPORT=8888
则真正的服务PORT=8066
也就是配置文件中的优先级大于环境变量
```
    所以当确定使用环境变量为服务IP+端口 时确保配置文件中没有此配置


#### 同一容器多个服务 


##### 问题
    当同一个容器中存在多个服务端口需要转发的时候，一个环境变量是不够的，比如容器环境变量中存在RPC_IP=10.0.5.56,RPC_PORT=8888
    此时这个容器中如果有两个服务的程序，都需要暴露端口，如果默认使用环境变量则它俩的服务的地址会是一样的，这样会导致错误。
##### 解决办法
    在第一个程序配置文件中加入以下内容 ：rpc.service.ip=$RPC_SER01_IP,pc.service.ip=$RPC_SER01_PORT

    在第二个程序配置文件中加入以下内容 ：rpc.service.ip=$RPC_SER02_IP,pc.service.ip=$RPC_SER02_PORT

    也就是说配置文件中可以指定此程序暴露服务的IP和端口所使用的环境变量名称，并不是直接配置死IP和端口