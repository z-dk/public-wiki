1. 引入jar包

```
 <!--swagger-->
       <dependency>
           <groupId>io.springfox</groupId>
           <artifactId>springfox-swagger2</artifactId>
           <version>2.6.1</version>
           <exclusions>
               <exclusion>
                   <groupId>org.springframework</groupId>
                   <artifactId>spring-beans</artifactId>
               </exclusion>
               <exclusion>
                   <groupId>org.springframework</groupId>
                   <artifactId>spring-context</artifactId>
               </exclusion>
               <exclusion>
                   <groupId>org.springframework</groupId>
                   <artifactId>spring-aop</artifactId>
               </exclusion>
           </exclusions>
       </dependency>
       <!--swagger前端文件-->
       <dependency>
           <groupId>io.springfox</groupId>
           <artifactId>springfox-swagger-ui</artifactId>
           <version>2.6.1</version>
       </dependency>
        <!--fweb-swagger前端文件-->
        <dependency>
		  <groupId>com.hhwy</groupId>
		  <artifactId>fweb-platform-swagger</artifactId>
		  <version>0.0.4</version>
		  <classifier>plugin</classifier>
		  <type>zip</type>
		  <exclusions>
			  <exclusion>
				  <groupId>*</groupId>
				  <artifactId>*</artifactId>
			  </exclusion>
		  </exclusions>
	  </dependency>
```
2. 在pom文件配置解压swagger的前端文件（在maven-dependency-plugin插件中解压开）

```
	<!-- 解压swagger前端文件 -->
		<artifactItem>
		    <groupId>com.hhwy</groupId>						
			<artifactId>fweb-platform-swagger</artifactId>
			<classifier>plugin</classifier>
			<type>zip</type>
			<outputDirectory>./src/main/webapp/</outputDirectory>
		</artifactItem>
```
如下，将上面的代码，粘贴到pom文件中相应的位置即可

```
<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-dependency-plugin</artifactId>
				<version>2.10</version>
				<executions>
					<!-- 拷贝jar -->
					<execution>
						<id>copy_jar</id>
						<phase>package</phase>
						<goals>
							<goal>copy-dependencies</goal>
						</goals>
						<configuration>
							<outputDirectory>./src/main/webapp/WEB-INF/lib</outputDirectory>
							<excludeTypes>zip</excludeTypes>
							<includeTypes>jar</includeTypes>
							<excludeScope>provided</excludeScope>
							<excludeScope>system</excludeScope>
						</configuration>
					</execution>
					<!-- 拷贝zip -->
					<!-- <execution>
						<id>copy_zip</id>
						<phase>package</phase>
						<goals>
							<goal>copy-dependencies</goal>
						</goals>
						<configuration>
							排除business-ac，不下载此zip
                            <excludeArtifactIds>business-ac</excludeArtifactIds>
							<outputDirectory>./src/main/webapp/plugins</outputDirectory>
							<excludeTypes>jar</excludeTypes>
							<includeTypes>zip</includeTypes>
							<stripVersion>true</stripVersion>
							<overWriteReleases>true</overWriteReleases>
						</configuration>
					</execution> -->
					<!-- 获取MANIFEST文件 -->
					<execution>
						<id>unpack</id>
						<phase>pre-integration-test</phase>
						<goals>
							<goal>unpack</goal>
						</goals>
						<configuration>
							<artifactItems>

								<!-- 解压swagger前端文件 -->
								<artifactItem>
									<groupId>com.hhwy</groupId>
									<artifactId>fweb-platform-swagger</artifactId>
									<classifier>plugin</classifier>
									<type>zip</type>
									<outputDirectory>./src/main/webapp/</outputDirectory>
								</artifactItem>

                        
							</artifactItems>
						</configuration>
					</execution>
				</executions>
			</plugin>
```


3. 加入配置文件


```
package com.hhwy.monitorplatform.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableWebMvc
@EnableSwagger2
@Configuration
public class SwaggerConfig {

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
               .apis(RequestHandlerSelectors.basePackage("com.hhwy.monitorplatform")) // 注意修改此处的包名，修改为项目所有controller上层的包名
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("接口列表 v1.1.0") // 任意，请稍微规范点
                .description("接口测试") // 任意，请稍微规范点
                .termsOfServiceUrl("http://localhost:8080/monitor-platform-userweb/swagger-ui.html") // 将“url”换成自己的ip:port
                .contact("why") // 无所谓（这里是作者的别称）
                .version("1.1.0")
                .build();
    }

}
```

4. 启动项目查看接口文档(项目名/swagger-ui.html访问)

```
http://localhost:8080/monitor-platform-userweb/swagger-ui.html
```
5.可以查看接口json数据

```
http://localhost:8080/monitor-platform-userweb/v2/api-docs
```



