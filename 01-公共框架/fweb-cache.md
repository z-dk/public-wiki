## fweb-cached 分布式缓存通用接口

### 简介

### 设计图 

![design image](https://git.365power.cn/jialx/documents/raw/master/images/fweb-cache.png)


### 主要功能有

* 快速使用缓存
* 可多种缓存同时使用
* 接口层与实现层分离
* 扩展非常方便，如果后期出来其他新缓存,只需要添加新实现就好
* 配置统一化
* 配置多元化，可从zk获取，prop文件获取，环境变量获取.... 后期还可以扩展更多的配置来源
* 实现类自动发现，从redis 换成 memcached 只需要把fweb-cached-redis.jar 换成 fweb-cached-memcached.jar 即可

### 扩展新缓存实现

1. 添加 XXXCacheClient 继承CacheClient抽象类 并实现其抽象方法。
2. 添加 XXXCacheClientPool 实现CacheClientPool接口 并实现其方法，
3. XXXCacheClientPool标记为@Component 添加到Spring context中
4. fweb-cached 启动后会自动推送配置并初始化CacheClientPool

### 扩展新的配置来源

1. 添加新的ConfigureFromXXXX  继承 ConfigureAbs 类并实现其方法。
2. fweb-cached启动后会自动 调用 getConfig, 配置获取到后需要开发者调用 本类initFactory  (像zk 这种异步的配置来源，平台没办法同步等待，只能配置到达后调用 initFactory)

### 使用缓存（配置文件方式 ，redis sentinel 为例）

##### 添加配置文件 fweb-cached.properties
```bash
#redis sentinel
hosts=10.0.2.9:5376,10.0.2.9:5377,10.0.2.9:5378,10.0.2.9:5379
redismaster=mymaster redis sentinel 特有配置，对其他缓存实现无效
maxconn=500 最大连接数
maxidle=30 最大空闲数
miniidle=10  最小空闲数
server=true 修改为true 后默认的配置会变高 ， 以上的除了hosts 其他的可以不用配置使用默认

```

#### 添加依赖坐标

```bash

<dependency>
			<groupId>com.hhwy</groupId>
			<artifactId>fweb-cached-api</artifactId>
			<version>0.0.1</version>
</dependency>
<dependency>
			<groupId>com.hhwy</groupId>
			<artifactId>fweb-cached-redis-sentinel</artifactId>
			<version>0.0.1</version>
</dependency>

```

#### 使用


`CacheClient client= CacheClientFactory.getInstance().getResource();`

##### 设置缓存

```bash
client.setString(key,value); 字符串
client.setString(key,value,timeout);
client.setBytes(key,value); 字节数组
client.setBytes(key,value,timeout);
client.setSerializable(key,value); 可序列化对象，bean，list，map 等
client.setSerializable(key,value,timeout);
```
##### 获取缓存

```bash
client.getString(key) 返回字符串
client.getSerializable(key,Class<T>) 返回该class 类型
client.getBytes(key) 返回字节数组
```
##### 删除缓存
    
`client.del("testStr");`

##### 关闭客户端 此方法一定要放到finally{}中，保证被执行

`client.close() 此方法为将连接返回给资源池。否则redis 会产生连接数耗尽的问题。（memcached 好像不会有问题，为了通用，使用后要统一关闭）`


### 多种缓存同时使用(以redis-sentinel，memcached为例)

如果工程中需要使用多种缓存请安以下方式配置

#### 首先需要知道使用的缓存的 hostsKey

缓存启动后会用这个key去获取对应的hosts,这个key是在实现缓存接口时向平台提供的。
```bash 
如redis sentinel的实现方法中为
 @Override
public String getHostsKey() {
		
	return "redis.sentinel.hosts";
}
```
```bash
memcached 的实现为
@Override
public String getHostsKey() {
		
	return "memcached.hosts";
}
```
#### 在配置中为该缓存配置hosts(以配置文件方式为例)

配置文件内容为
```bash
#redis sentinel
redis.sentinel.hosts=10.0.2.9:5376,10.0.2.9:5377,10.0.2.9:5378,10.0.2.9:5379
redismaster=mymaster
#memcached
memcached.hosts=10.0.2.9:11211,10.0.2.9:11222
maxconn=500
maxidle=30
miniidle=10
server=true
```
#### 使用Memcached ,传入要使用的类型

`CacheClient client= CacheClientFactory.getInstance().getResource(MemcachedCacheClientPool.class);`

#### 使用RedisSentinel ,传入要使用的类型

`CacheClient client= CacheClientFactory.getInstance().getResource(RedisSentinelCacheClientPool.class);`



### 使用Api自带本地缓存

开发的时候如果没有 redis ，memcached 等分布式缓存，可以使用 Api 自带的缓存。

自带缓存开启条件，只有当找不到其他可用缓存实现的时候本地缓存才会启用。（也就是没有找到其他缓存的实现.jar）.

自带缓存使用本地内存作为缓存介质，存储空间受限于JVM 内存。而且多个节点不会共享缓存。所以建议生产环境下尽量使用redis 等分布式缓存。

