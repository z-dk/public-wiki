## fweb-security 单点登录，权限管理框架

### 简介

fweb-security 使用 spring security 框架封装，使用Oauth2 标准协议进行单点登录，支持oauth2常用协议及自定义的扩展协议进行身份认证，返回token 使用 JSON Web Token（JWT） 令牌作为身份认证标识，


### Oauth2 协议支持

#### 相关概念

* Resource Owner：资源所有者。即用户。
* Client：客户端（第三方应用）。
* HTTP service：HTTP服务提供商，简称服务提供商。 
* User Agent：用户代理。本文中就是指浏览器。
* Authorization server：授权（认证）服务器。即服务提供商专门用来处理认证的服务器。
* Resource server：资源服务器，即服务提供商存放用户生成的资源的服务器。它与认证服务器，可以是同一台服务器，也可以是不同的服务器。
* Access Token：访问令牌。使用合法的访问令牌获取受保护的资源。


#### 授权码授权模式（Authorization code Grant）

授权码模式是功能最完整、使用最广泛、流程最严密的授权模式。由于这是一个基于重定向的流，所以客户端必须能够与资源所有者的用户代理(通常是web浏览器)进行交互，并且能够从授权服务器接收传入的请求(通过重定向)
![design image](https://git.365power.cn:8443/jialx/public-wiki/raw/master/images/oauth2-sqm.jpg)

* （A）用户访问客户端，客户端将用户导向授权服务器，通过用户代理（User-Agent）发送包括它的客户端标识符、请求的范围、本地状态和一个重定向URI，授权服务器在授予(或拒绝)访问权后将其发送给用户代理。
* （B）授权服务器对资源所有者进行身份验证(通过用户代理)，并确定资源所有者是否授予或拒绝客户端的访问请求。
* （C）假如资源所有者同意授权请求，那么授权服务器将会使用前面提供的或者事先指定的重定向URI（redirection URI），重定向到客户端，并附上一个授权码（code）和一个前面提供的本地状态（state）（如果有的话，则会原值返回）。
* （D）客户端收到授权码，附上早先的重定向URI，向授权服务器申请令牌。这一步是在客户端的后台的服务器上完成的，对用户不可见。在发出请求时，授权服务器对客户端进行身份验证。请求参数包含授权代码、用于获得验证的授权代码的重定向URI、标识客户端身份的client id和client secret。
* （E）授权服务器对客户端进行身份验证，验证授权代码，并确保所收到的重定向URI与用于在步骤(C)中对客户端重定向的URI相匹配，如果有效，授权服务器将发送访问令牌access token和刷新令牌refresh token（可选）。


#### 简化授权模式（Implicit Grant）
![design image](https://git.365power.cn:8443/jialx/public-wiki/raw/master/images/oauth2-yssqms.jpg)

简化模式（implicit grant type）不通过第三方应用程序的服务器，直接在浏览器中向认证服务器申请令牌，跳过了"授权码"这个步骤，因此得名。所有步骤在浏览器中完成，令牌对访问者是可见的，且客户端不需要认证。

#### 密码模式（Resource Owner Password Credentials Grant）
![design image](https://git.365power.cn:8443/jialx/public-wiki/raw/master/images/oauth2-mmms.jpg)

密码模式中，用户向客户端提供自己的用户名和密码。客户端使用这些信息，向"服务商提供商"索要授权。
在这种模式中，用户必须把自己的密码给客户端，但是客户端不得储存密码。这通常用在用户对客户端高度信任的情况下，比如客户端是操作系统的一部分，或者由一个著名公司出品。而认证服务器只有在其他授权模式无法执行的情况下，才能考虑使用这种模式

#### 客户端凭证模式（Client Credentials Grant）
![design image](https://git.365power.cn:8443/jialx/public-wiki/raw/master/images/oauth2-khdpz.jpg)

客户端模式（Client Credentials Grant）指客户端以自己的名义，而不是以用户的名义，向"服务提供商"进行认证。严格地说，客户端模式并不属于OAuth框架所要解决的问题。在这种模式中，用户直接向客户端注册，客户端以自己的名义要求"服务提供商"提供服务，其实不存在授权问题

#### 刷新Token 
![design image](https://git.365power.cn:8443/jialx/public-wiki/raw/master/images/oauth2-sxtk.jpg)

OAuth1.0的access token获取过来之后,就可以存到数据库里,然后长期使用,因为它有效期很长,通常有效期是无限的.但是OAuth2.0为了增强安全性,access token的有效期被大大缩短,通常只有几个小时,也可以申请增加到几十天,但是总是会有过期的时候.
为此,OAuth2.0增加了一个refresh token的概念,这个token并不能用于请求api.它是用来在access token过期后刷新access token的一个标记.这里所描述的场景,通常是指那种长周期的应用.也就是需要一直保持用户在线的应用.在线并不是说用户一直在用这个应用,也可能是用户已经离开,我们在后台仍然可以自动维持用户的状态.例如一个自动发状态的应用.用户并不需要操作这个应用,我们会定时在后台利用用户的accesskey帮助用户发送状态.这也算是用户维持登录状态的一种

#### 其他自定义模式

其他自定义模式如短信验证码等


### JWT 支持
JWT是一种用于双方之间传递安全信息的简洁的、URL安全的表述性声明规范,JWT作为一个开放的标准（RFC 7519)定义了一种简洁的，自包含的方法用于通信双方之间以JSON对象的形式安全的传递信息。因为数字签名的存在，这些信息是可信的,
* 简洁(Compact)：可以通过URL，POST参数或者在HTTP header发送，因为数据量小，传输速度也很快
* 自包含(Self-contained)：负载中包含了所有用户所需要的信息，避免了多次查询缓存或session
* JWT组成：JWT主要由三个部分组成：头部(HEADER)，载荷(PAYLOAD)，签证(SIGNATURE)
* 签证(SIGNATURE ): JWT的第三部分是一个签证信息，这个签证信息主要由三个部分组成：Header(BASE64后)，Payload(BASE64后)，secret。首先这个部分需要BASE64加密后的header和payload，然后使用进行连接组成的字符串，然后通过header中指定的加密方式，进行加盐值secret组合加密，然后就构成了JWT的第三部分

### 加密支持

* 支持RSA 非对称加密 ,主要用到web端等密码容易泄露的地方
* 支持AES、DES 对称加密,主要用在秘钥不容易泄露的地方
* 支持密码密文加密，多次加密同一内容密文不一致

### 功能介绍

单点登录：

用户管理：

权限管理：

资源管理：

角色管理：

子系统管理：

### 服务端使用

#### 服务端配置
```
#颁发访问令牌有效期(秒)
fweb.security.server.token.seconds=3600
#颁发刷新令牌有效期(秒)
fweb.security.server.refreshtoken.seconds=604800
#配置RSA 公钥
fweb.security.server.rsa.publickey=
#配置RSA私钥
fweb.security.server.rsa.privatekey=

```

### 客户端集成

#### 引入客户端依赖包

客户端使用返回结果应该全部使用api中的接口，方便后续扩展其他实现，client中会引入一套实现类fweb-security-datamodel

```
    <dependency>
        <groupId>com.hhwy</groupId>
        <artifactId>fweb-security-api</artifactId>
        <version>0.0.35</version>
    </dependency>
	<dependency>
        <groupId>com.hhwy</groupId>
        <artifactId>fweb-security-client</artifactId>
        <version>0.0.35</version>
    </dependency>
```
#### 客户端配置必要信息

```
#子系统ID
fweb.security.client.id=sso-server
#子系统secret
fweb.security.client.secret=sso-server-secret
#认证服务地址
fweb.security.server.url=http://ysd.365power.cn/fweb-security-server
#请求匹配规则，与下边的配置配合使用
fweb.security.client.redirect.matches=/view/**,/
#如果请求失败，将匹配规则匹配到的请求重定向到以下页面，否则返回401
fweb.security.client.redirect.path=/view/business-simple-mainframe-web/modules/login-unify/login
```

#### 配置过滤器

SpringBoot
```
	package com.hhwy.security.client.demo;
	import com.hhwy.security.sso.client.filter.SsoFilter;
	import org.springframework.context.annotation.Bean;
	import org.springframework.context.annotation.Configuration;
	import java.util.ArrayList;
	import java.util.List;
	/**
	* @BelongsProject: fweb-security
	* @BelongsPackage: com.hhwy.security.client.demo
	* @Author: jlx
	* @CreateTime: 2019-08-04 17:23
	* @Description: 配置过滤器
	*/
	@Configuration
	public class WebConfigure {
		@Bean
		public SsoFilter ssoFilter(){
			List<String> stringList=new ArrayList<>();
			SsoFilter filter = new SsoFilter(stringList);
			stringList.add("/**/*.js");
			stringList.add("/**/*.html");
			stringList.add("/fweb-security-client-demo/common/staticpath");
			stringList.add("/fweb-security-client-demo/login/**");
			return filter;
		}
	}
```
web.xml
```
	<filter>
		<filter-name>SsoFilter</filter-name>
		<filter-class>com.hhwy.security.sso.client.filter.SsoFilter</filter-class>
		<init-param>
			<param-name>whiteUrls</param-name>
			<param-value>
			/sso/client/**,
			/mainframe/imgcode,
			/mainframe/imgcodeValidate,
			/fweb-rest-service/invoke/**
			</param-value>
		</init-param>
	</filter>
	<filter-mapping>
		<filter-name>SsoFilter</filter-name>
		<url-pattern>/*</url-pattern>
	</filter-mapping>
```

#### 登录实例代码


``` html
前端,将用户名密码放入queryParams
login:function(){
	var me=this;
	$.ajax({
		url:basePath+'login',
		type : 'post',
		data : $.toJSON(me.queryParams),
		contentType : 'application/json;charset=UTF-8',
		success : function(result){
			console.log(result);
			debugger
			if(result.flag===true){
				location.href=basePath+'admin/list.html'
			}else{
				me.msg=result.message;
			}
		}
	})

}

```
``` java
后端

    @RequestMapping(path="/login",method = RequestMethod.POST)
	public AjaxResult login(@RequestBody Map<String,String> params, HttpServletRequest request, HttpServletResponse response) {

		String clientId=EnvPropertyConfigHelper.getValue("fweb.security.client.id");
		String clientSecret=EnvPropertyConfigHelper.getValue("fweb.security.client.secret");

		AjaxResultImpl loginResult=new AjaxResultImpl();
		AjaxResult ajaxResult=clientAdminService.loginByPassword(clientId,clientSecret,params.get("username"),params.get("password"),response);
		if(ajaxResult.isFlag()){
			String json=JSON.toJSONString(ajaxResult.getData());
			TokenInfo info= JSONObject.parseObject(json,TokenInfo.class);
			//使用token 获取 用户信息，返回
			loginResult.setData(info.getAccess_token());
		}
		loginResult.setFlag(ajaxResult.isFlag());
		loginResult.setMsg(ajaxResult.getMsg());

		return loginResult;
	}
```



#### 接口调用

Spring 引入ClientAdminService
```

	//---------------------------------------------------------登录接口------------------------------------------
	/**
	 * 登录接口
	 * @param clientId 子系统ID
	 * @param clientSecret 子系统票据
	 * @param userName 用户名明文
	 * @param password 密码明文
	 * @param response HttpServletResponse
	 * @return
	 */
	public AjaxResult loginByPassword(String clientId, String clientSecret, String userName, String password, HttpServletResponse response);


    /**
	 * 登录接口
	 * @param clientId 子系统ID
	 * @param clientSecret 子系统票据
	 * @param userName 用户名明文
	 * @param password 密码明文
	 * @param encryption 加密方式
	 * @param httpResponse HttpServletResponse
	 * @return
	 */
    public AjaxResult loginByPassword(String clientId, String clientSecret , String userName, String password,String encryption, HttpServletResponse httpResponse);

	/**
	 * 登出
	 * @return
	 */
	public AjaxResult logout(HttpServletRequest request, HttpServletResponse response);

    /**
     *  使用旧密码修改密码
     * @param param		loginName、oldPassword、newPassword 都必填
     * @return 
     * AjaxResult
     * <br/><b>创 建 人：</b>
     * <br/><b>创建时间:</b>
     * <br/><b>修 改 人：</b>wangzelu
     * <br/><b>修改时间:</b>2019年8月27日 上午11:22:47
     * @since  1.0.0
     */
    public AjaxResult changePasswordByOldPassword(Map<String,String> param);

	//---------------------------------------------------------登录人员信息接口------------------------------------------
	/**
	 * 获取当前登录的用户ID
	 * @return
	 */
	public String getLoginUserId();

	/**
	 * 获取当前登录的用户名
	 * @return
	 */
	public String getLoginUserName();

	/**
	 * 获取登录用户的组织机构id
	 * @return
	 */
	public String getLoginUserOrg();

	/**
	 * 获取当前登录用户信息
	 * @return
	 */
	public UserDetail getLoginUserDetail();

	/**
	 * 
	 * 根据登录人员id获取登录人员信息
	 * @param userId
	 * @return 
	 * UserDetail
	 * <br/><b>创 建 人：</b>zhouqi
	 * <br/><b>创建时间:</b>2019年8月14日 上午10:24:26
	 * <br/><b>修 改 人：</b>zhouqi
	 * <br/><b>修改时间:</b>2019年8月14日 上午10:24:26
	 * @since  1.0.0
	 */
	public UserDetail getUserByUserId(String userId);

	/**
	 * 方法描述: 据userName、logingNname、email、mobile精确查询用户基本信息
	 * <br/>@param userDetail
	 * <br/>@return 
	 * UserDetail
	 * <br/><b>创 建 人：</b>
	 * <br/><b>创建时间:</b>
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年8月30日 上午10:54:58
	 * @since  1.0.0
	 */
	public UserDetail getUserByParam(UserDetail userDetail);

	/**
	 * 更新用户
	 * @param userDetail
	 * @return
	 */
	public boolean updateUser(UserDetail userDetail);
	
	/**
     * 方法描述: 根据大集体单位id和大集体角色id获取单位下的此角色授权的人员
     * @param orgId		大集体组织机构id		(非必填)
     * @param roleId	大集体角色id			(必填)
     * @param includeChild		是否查询子集，当orgId有值的有效	(非必填)
     * @return 
     * List<UserDetail>
     * <br/><b>创 建 人：</b>zhangshuwei
     * <br/><b>创建时间:</b> 11:29 2019/7/24
     * <br/><b>修 改 人：</b>wangzelu
     * <br/><b>修改时间:</b>2019年8月26日 下午5:35:31
     * @since  1.0.0
     */
	public List<UserDetail> findUserByOrgAndRole(String orgId,String roleId,boolean includeChild);
	
	/**
	 * 方法描述: 根据大集体组织机构id获取其下所有用户信息(未停用的用户)
	 * 					只返回id, loginName, userName, createTime, mobile, email用户信息
	 * @param orgId				大集体组织机构id
	 * @param includeChild		是否级联查询组织机构子节点上的用户
	 * @return 
	 * List<UserDetail>
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>13:31 2019/7/24
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年8月26日 下午6:17:31
	 * @since  1.0.0
	 */
	public List<UserDetail> getUserByOrg(String orgId,boolean includeChild);
	
	/**
	 * @Author zhangshuwei
	 * 方法描述 据角色Id获取用户
	 * @Date 13:31 2019/7/24
	 * @Param is 是否查询子集
	 * @return
	 **/
	//已被findUserByOrgAndRole替代
//    public List<UserDetail> getUsersByRoleId(String roleId);
	
	/**
	 * 方法描述: 根据大用户（或客户）id获取大用户（或客户）的管理员信息（id,登录名）
	 * @param consOrCustId
	 * @return 
	 * UserDetail
	 * <br/><b>创 建 人：</b>zhaohongyue
	 * <br/><b>创建时间:</b>2019年8月13日17:35:30
	 * <br/><b>修 改 人：</b>zhaohongyue
	 * <br/><b>修改时间:</b>2019年8月13日17:35:30
	 * @since  1.0.0
	 */
	public UserDetail getAdminDetailByConsOrCustId(String consOrCustId);
	
	/**
	 * 
	 * 方法描述:(查询登录用户的id、名称、所属组织机构及 及上级的所属的单位)
	 * @param userId
	 * @return 
	 * UserDetail
	 * <br/><b>创 建 人：</b>zhouqi
	 * <br/><b>创建时间:</b>2019年8月14日 上午10:54:05
	 * <br/><b>修 改 人：</b>zhouqi
	 * <br/><b>修改时间:</b>2019年8月14日 上午10:54:05
	 * @since  1.0.0
	 */
	public UserDetail getOrgAndSuperDataByUserId(String userId);
	
	/**
	 * getAllUser: 获取所有用户								TODO:不能是所有用户，应该有个范围？ 看ac的接口实现是否是只查询当前登录人所属单位下的角色
	 * @return List<User> 用户列表
	 * @exception	
	 * @since  1.0.0	zhangshuwei
	 */
	public List<UserDetail> getAllUser();
	
	/**
	 * 方法描述: 根据组织与资源获取用户信息列表		TODO(???)
	 * @param orgId			组织id
	 * @param resId			资源id
	 * @param queryChildren	是否查询组织及下级的用户
	 * @return 
	 * List<UserDetail>		用户信息列表
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>
	 * <br/><b>修 改 人：</b>
	 * <br/><b>修改时间:</b>
	 * @since  1.0.0
	 */
	public List<UserDetail> getUserByOrgAndRes(String orgId,String resId,boolean queryChildren);

	/**
	 * 方法描述: 根据ids 获取人员
	 * @param uids			人员id
	 * @return
	 * List<UserDetail>		用户信息列表
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>
	 * <br/><b>修 改 人：</b>
	 * <br/><b>修改时间:</b>
	 * @since  1.0.0
	 */
	public List<UserDetail> getUserByIds(List<String> uids);


	//---------------------------------------------------------角色相关------------------------------------------
	/**
	 * 方法描述:据组织Id获取大集体角色分页信息
	 * 					查询规则，查询出此组织机构节点的所有角色，和此组织机构节点所有父级节点上的通用角色
	 * @param params		orgId： 组织机构id   String（必填）
	 * 						status： 角色状态：  	String  0:正常角色，1:注销角色（非必填,默认是0，查询正常角色）
	 * 						roleName： 角色名称/编码， String  （模糊查询，非必填）
	 * 						includeChild： 是否级联查询子节点  Bollean: true或false（非必填，默认不级联）
	 * 						pagingFlag：是否分页    Bollean: true或false（非必填，默认不分页）
	 * 						page：页码			Integer
	 * 						rows：每页条数		Integer
	 * <br/>@param params
	 * <br/>@return 
	 * AjaxPagingResult<RoleDetail>
	 * <br/><b>创 建 人：</b>wangzelu
	 * <br/><b>创建时间:</b>2019年9月3日 下午3:40:51
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年9月3日 下午3:40:51
	 * @since  1.0.0
	 */
	public AjaxPagingResult<RoleDetail> listRoleDetialByParams(Map<String, Object> params);
	
	/**
	 * 方法描述: 根据用户登录人员id（大用户人员id或者大集体人员id）获取用户已授权的角色
	 * @param userId	
	 * @return 
	 * Object
	 * <br/><b>创 建 人：</b>wangzelu
	 * <br/><b>创建时间:</b>2019年8月13日 下午6:26:41
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年8月13日 下午6:26:41
	 * @since  1.0.0
	 */
	public List<RoleDetail>  getRolesOfUserByUserId(String userId);
	
	/**
	 * getAllRoles: 获取所有角色							TODO:不能是所有角色，应该有个范围？ 看ac的接口实现是否是只查询当前登录人所属单位下的角色
	 * @return List<Map<String,Object>> 角色列表信息
	 * @exception
	 * @since  1.0.0
	 */
	public List<Map<String,Object>> getAllRoles();
	
	/**
	 * 方法描述: 根据组织机构获取角色
	 * @param orgId				组织机构id
	 * @param queryChildren		是否查询子级
	 * @param hasCommonRole		是否包含其他组织的通用角色
	 * @return 
	 * List<Map<String,Object>>
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>
	 * <br/><b>修 改 人：</b>
	 * <br/><b>修改时间:</b>
	 * @since  1.0.0
	 */
	public List<Map<String,Object>> getRoleByOrgId(String orgId,boolean queryChildren,boolean hasCommonRole);
	
	/**
	 * 方法描述: 根据资源code列表，获取拥有此资源权限的角色信息
	 * <br/>@param resCodeList		资源code列表
	 * <br/>@return 
	 * List<Map<String,String>>		角色的id,name,code,isCommon,type,description
	 * <br/><b>创 建 人：</b>
	 * <br/><b>创建时间:</b>
	 * <br/><b>修 改 人：</b>
	 * <br/><b>修改时间:</b>
	 * @since  1.0.0
	 */
	public List<Map<String,String>> getRoleByRes(List<String> resCodeList);

	/**
	 * 方法描述: 根据roleIds 获取角色信息
	 * @param ids 角色id
	 * @return
	 * OrgDetail
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>13:20 2019/7/24
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年8月26日 下午5:40:21
	 * @since  1.0.0
	 */
	public List<RoleDetail> getRoleDetailsByRoleIds(List<String> ids);

	/**
	 * 方法描述: 根据角色名称 获取角色信息
	 * @param name 角色名称
	 * @return
	 * OrgDetail
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>13:20 2019/7/24
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年8月26日 下午5:40:21
	 * @since  1.0.0
	 */
	public List<RoleDetail> getRoleDetailsByRoleName(String name);


	
	//---------------------------------------------------------大集体组织机构相关------------------------------------------
	/**
	 * 方法描述: 根据组织机构编码获取组织机构
	 * @param code
	 * @return 
	 * OrgDetail
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>13:20 2019/7/24
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年8月26日 下午5:40:21
	 * @since  1.0.0
	 */
	public OrgDetail getOrgByCode(String code);
	
	/**
	 * 方法描述: 根据 org的id 获取单独的组织机构信息
	 * @param orgId
	 * @return 
	 * OrgDetail
	 * <br/><b>创 建 人：</b>zhaohonghao
	 * <br/><b>创建时间:</b>2019年8月13日17:35:30
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年8月27日 下午7:03:57
	 * @since  1.0.0
	 */
	public OrgDetail getOrgDetailById(String orgId);
	
	/**
	 * 方法描述: 根据 org的idList 获取组织机构信息
	 * @param idList
	 * @return 
	 * List<OrgDetail>
	 * <br/><b>创 建 人：</b>zhaohonghao
	 * <br/><b>创建时间:</b>2019年8月13日17:35:30
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年8月27日 下午7:07:39
	 * @since  1.0.0
	 */
	public List<OrgDetail> getOrgDataByOrgIdList(List<String> idList);
	
	/**
	 * 方法描述: 根据登录的大集体用户ID查询  拥有数据权限 的 组织机构id列表
	 * @param userId	登录人员id（非必填，默认登录人id）
	 * @return 
	 * List<String>
	 * <br/><b>创 建 人：</b>zhaohonghao
	 * <br/><b>创建时间:</b>2019年8月13日17:35:30
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年8月27日 上午11:25:09
	 * @since  1.0.0
	 */
	public List<String> getOrgIdListByUserId(String userId);
	
	/**
	 * 方法描述: 根据登录的大集体用户ID查询  拥有数据权限 的 orgPid下的组织机构id列表
	 * <br/>@param userId	登录人员id（非必填，默认登录人id）
	 * <br/>@param orgPid	组织机构id（非必填，若为null，则获取所有有权限的组织id）
	 * <br/>@return 
	 * List<String>
	 * <br/><b>创 建 人：</b>wangzelu
	 * <br/><b>创建时间:</b>2019年8月29日 上午10:57:23
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年8月29日 上午10:57:23
	 * @since  1.0.0
	 */
	public List<String> getOrgIdListByUserIdAndOrgPid(String userId, String orgPid);
	
	/**
	 * 方法描述: 根据登录的大集体用户ID查询  拥有数据权限 的 组织机构detail列表
	 * <br/>@param userId	登录人员id（非必填，默认登录人id）
	 * <br/>@param orgPid	组织机构id（非必填，若为null，则获取所有有权限的组织detail）
	 * <br/>@return 
	 * List<OrgDetail>		拥有数据权限的组织机构detailList
	 * <br/><b>创 建 人：</b>wangzelu
	 * <br/><b>创建时间:</b>2019年8月29日 上午10:44:08
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年8月29日 上午10:44:08
	 * @since  1.0.0
	 */
	public List<OrgDetail> getOrgDetailListByUserIdAndOrgPid(String userId, String orgPid);
	
	/**
	 * 方法描述: 根据登录的大集体用户ID查询  拥有数据权限 的 组织机构树形结构
	 * 				TreeNode中的attributes中含有hasAuthzFlag标识是否拥有此组织权限
	 * 					hasAuthzFlag：true：代表有此组织机构的权限；false：没有此组织机构的权限
	 * <br/>@param userId	登录人员id（非必填，默认登录人id）
	 * <br/>@param orgPid	组织机构id（非必填，若为null，则获取所有有权限的组织节点）
	 * <br/>@return 
	 * List<OrgDetail>		拥有数据权限的组织机构树形结构（包含：拥有数据权限的节点及其父节点直至根节点）
	 * <br/><b>创 建 人：</b>wangzelu
	 * <br/><b>创建时间:</b>2019年8月29日 上午10:44:08
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年8月29日 上午10:44:08
	 * @since  1.0.0
	 */
	public List<TreeNode> getOrgDetailTreeByUserIdAndOrgPid(String userId, String orgPid);
	
	/**
	 * 方法描述: 根据组织机构id获取其下的组织机构列表
	 * 				此方法不涉及任何数据过滤，是为client接口提供的。
	 * <br/>@param orgPid	组织机构id (0代表获取所有)
	 * <br/>@return 
	 * List<OrgDetail>			attributes里有code, type, level
	 * <br/><b>创 建 人：</b>wangzelu
	 * <br/><b>创建时间:</b>2019年8月30日 上午10:36:18
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年8月30日 上午10:36:18
	 * @since  1.0.0
	 */
	public List<OrgDetail> getOrgDetailListByOrgPid(String orgPid);
	
	/**
	 * 方法描述: 根据组织机构id获取其下的组织机构树
	 * 				此方法不涉及任何数据过滤，是为client接口提供的。
	 * <br/>@param orgPid	组织机构id
	 * <br/>@return 
	 * List<TreeNodeImpl>		attributes里有code, type, level
	 * <br/><b>创 建 人：</b>wangzelu
	 * <br/><b>创建时间:</b>2019年8月29日 下午7:14:56
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年8月29日 下午7:14:56
	 * @since  1.0.0
	 */
	public List<TreeNode> getOrgTreeByOrgPid(String orgPid);
	
	/**
	 * 方法描述: 根据pid获取其下的组织机构树   TODO：实现需要变更
	 * @param parentOrgId		大集体组织机构id			String parentOrgId
	 * @return 
	 * List<TreeNode>
	 * <br/><b>创 建 人：</b>wangzelu
	 * <br/><b>创建时间:</b>2019年8月26日 上午10:27:36
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年8月26日 上午10:27:36
	 * @since  1.0.0
	 */
	public List<TreeNode> getOrgTree(Map<String, Object> params);
	
	/**
	 * 方法描述: 获取当前登录人的所在公司（大集体组织机构中type为0的）
	 * @param userId		若为null，则获取当前登录人的，若不为null则根据userId查询
	 * @return 
	 * OrgDetail
	 * <br/><b>创 建 人：</b>wangzelu
	 * <br/><b>创建时间:</b>2019年8月20日 上午9:17:23
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年8月20日 上午9:17:23
	 * @since  1.0.0
	 */
	public OrgDetail getCompanyOrgDetailOfLoginUser(String userId);
	
	/**
	 * 方法描述: 根据客户id集合查询客户所属的组织信息
	 * @param custIds 客户id集合		
	 * @return 
	 * OrgDetail
	 * <br/><b>创 建 人：</b>zhouqi
	 * <br/><b>创建时间:</b>2019年8月20日 上午9:17:23
	 * <br/><b>修 改 人：</b>zhouqi
	 * <br/><b>修改时间:</b>2019年8月20日 上午9:17:23
	 * @since  1.0.0
	 */
	public List<OrgDetail> getOrgDetailListByCustIdList(String[] custIds);
	
	//---------------------------------------------------------客户和大用户相关------------------------------------------
	
	/**
	 * 方法描述: 根据大集体登陆用户Id，获取当前组织及子组织下的客户下的所有大用户信息
	 * 					OrgDetail下的List<CustomDetail> list为代理的所有客户信息，CustomDetail中的List<CustTreeDetail> list为客户拥有的大用户信息
	 * @param userId
	 * @return 
	 * OrgDetail
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>8:35 2019/8/14
	 * <br/><b>修 改 人：</b>zhangshuwei
	 * <br/><b>修改时间:</b>8:35 2019/8/14
	 * @since  1.0.0
	 */
	public OrgDetail getOrgAllInformation(String userId);
	
	/**
	 * 方法描述: 根据用户Id(不区分大集体和客户、大用户)，获取当前组织及子组织下的客户下的所有“大用户”信息
	 * 				若是大集体人员：先找到人员拥有的数据权限组织id，获取其下的代理的所有所有大用户id
	 * 				若是大用户人员：先找到此大用户的客户的代理公司id，然后获取此组织机构下代理的所有大用户id
	 * @param usrId		TODO: 加查询参数：, Map<String, String> params
	 * @return 
	 * List<AcConsumerDetail>
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>17:33 2019/8/16
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年8月19日 上午11:14:48
	 * @since  1.0.0
	 */
	public List<AcConsumerDetail> getAllAcConsumerDetailByUserId(String usrId);
	
	/**
	 * 方法描述: 
	 * <br/>@param params		参数都不是必填
	 * 							userId: 登录人员id	查询规则 （根据用户Id(不区分大集体和客户、大用户)，获取当前组织及子组织下的客户下的所有“大用户”信息
	 * 														若是大集体人员：先找到人员拥有的数据权限组织id，获取其下的代理的所有所有大用户id
	 * 														若是大用户人员：先找到此大用户的客户的代理公司id，然后获取此组织机构下代理的所有大用户id,
	 * 																若同时存在orgId参数，以userId得出的orgId为准）
	 * 							orgId: 大集体组织机构id（查询其下的大用户）
	 * 							consName: 大用户名称（模糊查询）
	 * 							code： 用户编号（模糊查询）
	 * 							asyncTeam：代维队伍（代维班组），（是外键id，精确查询）
	 * 							importantGrade：重要等级（码表，精确查询）
	 * 							pagingFlag：是否分页，
	 * 							page：页码
	 * 							rows：每页条数
	 * <br/>@return 
	 * AjaxPagingResult<AcConsumerDetail>
	 * <br/><b>创 建 人：</b>wangzelu
	 * <br/><b>创建时间:</b>2019年9月2日 下午6:54:12
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年9月2日 下午6:54:12
	 * @since  1.0.0
	 */
	public AjaxPagingResult<AcConsumerDetail> pageAcConsumerDetailByParams(Map<String, String> params);
	
	/**
	 * 
	 * 方法描述:(根据登录人id，获取大用户信息、所属的客户信息和所属大集体的信息)
	 * @param userId
	 * @return 
	 * AcConsumerDetail
	 * <br/><b>创 建 人：</b>zhouqi
	 * <br/><b>创建时间:</b>2019年8月14日 下午2:32:01
	 * <br/><b>修 改 人：</b>zhouqi
	 * <br/><b>修改时间:</b>2019年8月14日 下午2:32:01
	 * @since  1.0.0
	 */
	public AcConsumerDetail getConsAndCustAndOrgDataByUserId(String userId);
	
	/**
	 * 
	 * 方法描述:(根据客户登陆人员Id，获取客户的信息，及其下所有的大用户信息，所属大集体的信息)
	 * @param userId
	 * @return 
	 * AcCustomerDetail
	 * <br/><b>创 建 人：</b>zhouqi
	 * <br/><b>创建时间:</b>2019年8月14日 下午3:30:05
	 * <br/><b>修 改 人：</b>zhouqi
	 * <br/><b>修改时间:</b>2019年8月14日 下午3:30:05
	 * @since  1.0.0
	 */
	public AcCustomerDetail getCustAndConsListAndOrgDataByUserId(String userId);

	/**
	 *
	 * 方法描述:(根据大用户id获取大用户信息、所属的客户信息)
	 * @param consId
	 * @return
	 * AcConsumerDetail
	 * <br/><b>创 建 人：</b>zhaohongyue
	 * <br/><b>创建时间:</b>2019年8月31日 上午11:40:00
	 * <br/><b>修 改 人：</b>zhaohongyue
	 * <br/><b>修改时间:</b>2019年8月31日 上午2:32:01
	 * @since  1.0.0
	 */
	public AcConsumerDetail getConsAndCustByConsId(String consId);
	
	/**
	 * 方法描述: 根据大用户名称获取大用户
	 * @return
	 *
	 * <br/><b>创 建 人：</b>zhaohongyue
	 * <br/><b>创建时间:</b>2019年8月26日 下午14:23:22
	 * <br/><b>修 改 人：</b>zhaohongyue
	 * <br/><b>修改时间:</b>2019年8月26日 下午14:23:22
	 * @since  1.0.0
	 */
	public AcConsumerDetail getAcConsumerDetailByName(String name);

	/**
	 * 方法描述: 根据单位获取该单位下的客户  orgid page rows
	 * @return			TODO:  这个方法应该改成调用pageAcConsumerDetailByParams -- by wangzelu
	 * Map<String,Object>
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>2019年8月26日 下午16:13:22
	 * <br/><b>修 改 人：</b>zhangshuwei
	 * <br/><b>修改时间:</b>2019年8月26日 下午16:13:22
	 * @since  1.0.0
	 */
	@Deprecated
	public Map<String,Object> getAcCustomerDetails(Map<String,Object> map);

	/**
	 * 方法描述: 根据单位获取该单位下的客户下拉框  ，不级联子节点
	 *							这个方法应该改成调用pageAcConsumerDetailByParams -- by wangzelu
	 * @param map			orgid：组织机构id（必填）
	 * @return Map<String, Object>
	 * <br/><b>创 建 人：</b>zhaohongyue
	 * <br/><b>创建时间:</b>2019年8月31日 下午16:13:22
	 * <br/><b>修 改 人：</b>zhaohongyue
	 * <br/><b>修改时间:</b>2019年8月31日 下午16:13:22
	 * @since 1.0.0
	 */
	@Deprecated
	public List<Map<String,Object>> getAcCustomerDetailsValueAndName(Map<String,Object> map);

	/**
	 * 方法描述: 根据客户id获取客户详情
	 * @return
	 * AcCustomerDetail
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>2019年8月26日 下午16:23:22
	 * <br/><b>修 改 人：</b>zhangshuwei
	 * <br/><b>修改时间:</b>2019年8月26日 下午16:23:22
	 * @since  1.0.0
	 */
	public AcCustomerDetail getAcCustomerDetail(String custId);

	/**
	 * 方法描述: 根据userId获取客户用户的树
	 * @return
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>2019年8月26日 下午18:33:22
	 * <br/><b>修 改 人：</b>zhangshuwei
	 * <br/><b>修改时间:</b>2019年8月26日 下午18:33:22
	 * @since  1.0.0
	 */
	public List<TreeNode> getAcCustomerAndAcConsumerTree(String userId);

	/**
	 * 方法描述: 根据consId获取用户详情
	 * @return
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>2019年8月26日 下午17:33:22
	 * <br/><b>修 改 人：</b>zhangshuwei
	 * <br/><b>修改时间:</b>2019年8月26日 下午17:33:22
	 * @since  1.0.0
	 */
	public AcConsumerDetail getAcConsumerDetailByConsId(String consId);
	
	/**
	 * 方法描述: 根据组织机构ID获取客户下的用户  orgid page rows
	 * @return
	 * Map<String,Object>
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>2019年8月26日 下午16:13:22
	 * <br/><b>修 改 人：</b>zhangshuwei
	 * <br/><b>修改时间:</b>2019年8月26日 下午16:13:22
	 * @since  1.0.0
	 */
	public Map<String, Object> getAcConsumerByOrgId(Map<String, Object> map);
	
	/**
	 * 
	 * @Title: getAcConsumerDetailListByParams
	 * @Description:(根据条件查询用户信息)
	 * @param ids:用户id集合；name：用户名称
	 * @return 
	 * List<AcConsumerDetail>
	 * <b>创 建 人：</b>zhouqi<br/>
	 * <b>创建时间:</b>2019年8月30日 上午11:16:19
	 * <b>修 改 人：</b>zhouqi<br/>
	 * <b>修改时间:</b>2019年8月30日 上午11:16:19
	 * @since  1.0.0
	 */
	public List<AcConsumerDetail> getAcConsumerDetailListByParams(Map<String,Object> params);
	
	/**
	 * 方法描述: 新增客户信息
	 * <br/>@param acCustomerDetail
	 * <br/>@return 
	 * String
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>2019年8月26日 下午16:33:22
	 * <br/><b>修 改 人：</b>zhangshuwei
	 * <br/><b>修改时间:</b>2019年8月26日 下午16:33:22
	 * @since  1.0.0
	 */
	public String addAcCustomer(AcCustomerDetail acCustomerDetail);
	
	/**
	 * 方法描述: 修改客户信息
	 * @return
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>2019年8月26日 下午16:33:22
	 * <br/><b>修 改 人：</b>zhangshuwei
	 * <br/><b>修改时间:</b>2019年8月26日 下午16:33:22
	 * @since  1.0.0
	 */
	public String updateAcCustomer(AcCustomerDetail acCustomerDetail);

	/**
	 * 方法描述: 添加客户  ,全量添加（包含security中不能维护的附加信息） 
	 * <br/>@param acCustomerDetail 
	 * void
	 * <br/><b>创 建 人：</b>wangzhichao
	 * <br/><b>创建时间:</b>2019年8月29日 下午6:57:57
	 * <br/><b>修 改 人：</b>wangzhichao
	 * <br/><b>修改时间:</b>2019年8月29日 下午6:57:57
	 * @since  1.0.0
	 */
	public void insertAcCustomerComplete(AcCustomerDetail acCustomerDetail);
	

	/**
	 * 方法描述: 客户更新  ,全量更新（包含security中不能维护的附加信息）
	 * <br/>@param acCustomerDetail 
	 * void
	 * <br/><b>创 建 人：</b>wangzhichao
	 * <br/><b>创建时间:</b>2019年8月29日 下午6:57:37
	 * <br/><b>修 改 人：</b>wangzhichao
	 * <br/><b>修改时间:</b>2019年8月29日 下午6:57:37
	 * @since  1.0.0
	 */
	public void updateAcCustomerComplete(AcCustomerDetail acCustomerDetail);
	
	/**
	 * 方法描述:  添加用户
	 * @return
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>2019年8月26日 下午17:43:22
	 * <br/><b>修 改 人：</b>zhangshuwei
	 * <br/><b>修改时间:</b>2019年8月26日 下午17:43:22
	 * @since  1.0.0
	 */
	public String addAcConsumerDetail(AcConsumerDetail AcCustomerDetail);
	
	/**
	 * 方法描述: 修改用户信息
	 * @return
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>2019年8月26日 下午17:33:22
	 * <br/><b>修 改 人：</b>zhangshuwei
	 * <br/><b>修改时间:</b>2019年8月26日 下午17:33:22
	 * @since  1.0.0
	 */
	public String updateAcConsumerDetail(AcConsumerDetail AcCustomerDetail);
	
	/**
	 * 方法描述: 添加用户  ,全量添加（包含security中不能维护的附加信息） 
	 * <br/>@param acConsumerDetail 
	 * void
	 * <br/><b>创 建 人：</b>wangzhichao
	 * <br/><b>创建时间:</b>2019年8月29日 下午6:58:58
	 * <br/><b>修 改 人：</b>wangzhichao
	 * <br/><b>修改时间:</b>2019年8月29日 下午6:58:58
	 * @since  1.0.0
	 */
	public void insertAcConsumerComplete(AcConsumerDetail acConsumerDetail);
	
	/**
	 * 方法描述: 用户的更新  ,全量更新（包含security中不能维护的附加信息） 
	 * <br/>@param acConsumerDetail 
	 * void
	 * <br/><b>创 建 人：</b>wangzhichao
	 * <br/><b>创建时间:</b>2019年8月29日 下午2:15:26
	 * <br/><b>修 改 人：</b>wangzhichao
	 * <br/><b>修改时间:</b>2019年8月29日 下午2:15:26
	 * @since  1.0.0
	 */
	public void updateAcConsumerComplete(AcConsumerDetail acConsumerDetail);
	
	/**
	 * 方法描述: 删除用户
	 * @return
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>2019年8月26日 下午17:53:22
	 * <br/><b>修 改 人：</b>zhangshuwei
	 * <br/><b>修改时间:</b>2019年8月26日 下午17:53:22
	 * @since  1.0.0
	 */
	public void deleteAcConsumerDetail(String consId);
	
	/**
	 * 方法描述: 删除客户信息
	 * @return
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>2019年8月26日 下午18:33:22
	 * <br/><b>修 改 人：</b>zhangshuwei
	 * <br/><b>修改时间:</b>2019年8月26日 下午18:33:22
	 * @since  1.0.0
	 */
	public void deleteAcCustomer(String cusId);
	
	//---------------------------------------------------------资源相关------------------------------------------
	
	/**
	 * 方法描述: 根据大集体（或大用户）登陆用户Id,获取有权限的子系统的资源树列表(树结构)（资源url为完整url，包括域名和项目名）
	 * @param userId
	 * @return 
	 * List<?>
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>11:12 2019/8/14
	 * <br/><b>修 改 人：</b>zhangshuwei
	 * <br/><b>修改时间:</b>11:12 2019/8/14
	 * @since  1.0.0
	 */
	public List<MenuBeanDetail> getResourcesListTree(String userId);
	
	/**
	 * 方法描述: 根据大集体（或大用户）登陆用户Id,获取有权限的资源树列表(树结构)（资源url为完整url，包括域名和项目名）
	 * 				和getResourcesListTree接口区别是：没有通过ac_user_subsystem_rela（用户和平台关系表）过滤资源，为营销系统提供（营销系统没有子系统的概念）
	 * <br/>@param userId 		人员id（非必填，默认当前登录人）
	 * <br/>@return 
	 * List<MenuBeanDetail>
	 * <br/><b>创 建 人：</b>wangzelu
	 * <br/><b>创建时间:</b>2019年8月29日 下午9:52:22
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年8月29日 下午9:52:22
	 * @since  1.0.0
	 */
	public List<MenuBeanDetail> getResourcesListTreeIgnoreSubSystem(String userId);
	
	
	/**
	 * 方法描述: 根据大集体（或大用户）登陆用户Id、资源类型、资源父id，级联获取资源列表（资源url为完整url，包括域名和项目名）
	 * <br/>@param userId			人员id（非必填，默认当前登录人）
	 * <br/>@param resourceType		资源类型：0菜单，1按钮，2流程资源  （非必填，默认为0：菜单资源）
	 * <br/>@param resPid			资源父id，级联子节点(非必填,默认查所有)
	 * <br/>@return 
	 * List<ResourceDetail>
	 * <br/><b>创 建 人：</b>wangzelu
	 * <br/><b>创建时间:</b>2019年8月29日 下午8:11:21
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年8月29日 下午8:11:21
	 * @since  1.0.0
	 */
	public List<ResourceDetail> getResourcesList(String userId, String resourceType, String resPid);
	
	/**
	 * 方法描述: 	根据id获取资源
	 * @param resId		资源id
	 * @return 
	 * ResourceDetail		资源对象
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>
	 * <br/><b>修 改 人：</b>
	 * <br/><b>修改时间:</b>
	 * @since  1.0.0
	 */
	public ResourceDetail getResById(String resId);
	
	
	
	
	
	
	

	


	

	
	

	
	
	
	
	
	
	//------------------------------------------  以下为营销接口（原sdk接口）  ------------------------------------------------
	
	/**
	 * 
	 * 方法描述:(保存组织机构信息)
	 * @param orgDetail：组织机构对象
	 * @return 
	 * void
	 * <br/><b>创 建 人：</b>zhouqi
	 * <br/><b>创建时间:</b>2019年8月23日 下午5:41:43
	 * <br/><b>修 改 人：</b>zhouqi
	 * <br/><b>修改时间:</b>2019年8月23日 下午5:41:43
	 * @since  1.0.0
	 */
	public void saveOrg(OrgDetail orgDetail);
	
	/**
	 * 方法描述: 根据组织与资源获取用户
	 * @param orgId 组织机构id
	 * @param resCode 资源id或资源编码
	 * @param queryParent 是否查询组织及上级的用户
	 * @return 
	 * List<User>
	 * <br/><b>创 建 人：</b>lilei
	 * <br/><b>创建时间:</b>2018年2月24日 下午1:14:49
	 * <br/><b>修 改 人：</b>lilei
	 * <br/><b>修改时间:</b>2018年2月24日 下午1:14:49
	 * @since  1.0.0
	 */
	public List<UserDetail> getUserListByOrgAndRes(String orgId, String resCode, boolean queryParent);
	
	/**
	 * 方法描述: 获取直属部门及当前组织机构下的对应角色的所有用户
	 * @param orgId 组织机构Id
	 * @param roleId 角色Id
	 * @return </br>
	 * @author
	 * <br/><b>创  建   人：</b>zhaoweidong
	 * <br/><b>创建时间：</b>2018年6月26日 上午9:33:22
	 * <br/><b>修   改  人：</b>wangzelu
	 * <br/><b>修改时间：</b>2019-09-03 11:06 ---暂时没用的
	 * @since  1.0.0
	 */
//	public List<UserDetail> getDirectUserByOrgAndRole(String orgId,String roleId) throws ServerHttpException;
	
	/**
	 * 方法描述: 根据组织机构ID和角色编码获取用户列表
	 * 
	 * @param orgId 组织机构Id
	 * @param roleCode 角色编码
	 * @return </br>
	 * @author
	 * <br/><b>创  建   人：</b>zhaoweidong
	 * <br/><b>创建时间：</b>2018年8月23日 下午4:09:37
	 * <br/><b>修   改  人：</b>wangzelu
	 * <br/><b>修改时间：</b>2019-09-03 11:06 ---暂时没用的
	 * @since  1.0.0
	 */
//	public List<UserDetail> getDirectUserByOrgIdAndRoleCode(String orgId,String roleCode) throws ServerHttpException;
	
	/**
	 * 方法描述: 根据组织机构或用户id获取组织机构集合
	 * orgId不为空，userId为空，获取该组织机构下的所有下级
	 * orgId和userId都不为空时，获取该用户在该组织机构下的有权限的所有下级
	 * @param orgId
	 * @param userId
	 * @return </br>
	 * @author
	 * <br/><b>创  建   人：</b>zhaoweidong
	 * <br/><b>创建时间：</b>2018年12月17日 下午4:39:04
	 * <br/><b>修   改  人：</b>zhaoweidong
	 * <br/><b>修改时间：</b>2018年12月17日 下午4:39:04
	 * @since  1.0.0
	 */
	public List<OrgDetail> getSubOrgListByOrgId(String orgId,String userId,Boolean childFlag,String type);
	
	/**
	 * 
	 * 方法描述: 
	 * @param orgId
	 * @param userId
	 * @return </br>
	 * @author
	 * <br/><b>创  建   人：</b>zhaoweidong
	 * <br/><b>创建时间：</b>2018年12月17日 下午4:39:00
	 * <br/><b>修   改  人：</b>zhaoweidong
	 * <br/><b>修改时间：</b>2018年12月17日 下午4:39:00
	 * @since  1.0.0
	 */
	public List<String> getSubOrgIdListByOrgId(String orgId,String userId,Boolean childFlag,String type);
	
	/**
	 * 根据id获取下级组织机构字符串（sql查询使用）
	 * 方法描述: 
	 * @param orgId 组织机构Id
	 * @param userId 用户Id（不为空时取数据权限合集）
	 * @return </br>
	 * @author
	 * <br/><b>创  建   人：</b>zhaoweidong
	 * <br/><b>创建时间：</b>2018年12月17日 下午4:38:53
	 * <br/><b>修   改  人：</b>zhaoweidong
	 * <br/><b>修改时间：</b>2018年12月17日 下午4:38:53
	 * @since  1.0.0
	 */
	public String getSubOrgIdStrForSqlByOrgId(String orgId,String userId,Boolean childFlag,String type);
	
	/**
	 * 
	 * 方法描述:(根据id获取下级组织机构字符串)
	 * @param params：  ids：id数组；userIds：登录用户id数组；types：类型数组；childFlag：是否查询子节点（true/false），pid：父节点id；idPath：id路径；level：级别
	 * @return 
	 * String
	 * <br/><b>创 建 人：</b>zhouqi
	 * <br/><b>创建时间:</b>2019年8月23日 下午8:52:46
	 * <br/><b>修 改 人：</b>zhouqi
	 * <br/><b>修改时间:</b>2019年8月23日 下午8:52:46
	 * @since  1.0.0
	 */
	public String getSubOrgIdStrForSqlByParams(Map<String,Object> params) throws ServerHttpException;
	
	/**
	 * 方法描述: 根据参数获取人员。获取符合关键字的父级(包含自身)下面的符合关键字的用户。（不考虑数据过滤）
	 * 					首先根据userId或者orgId确定基准组织机构（baseOrg），然后再找到baseOrg符合orgParentsKeywordCodeList所有关键字的父级org，
	 * 					然后在此父级org下找到符合roleKeywordCodeList和userKeywordCodeList所有关键字的user，并返回
	 * 				参数都为非必传的。
	 * @param keywordMap	含有3个键值对
	 * 					orgParentsKeywordCodeList	父级组织机构的关键字
	 * 					roleKeywordCodeList			角色的关键字
	 * 					userKeywordCodeList			用户的关键字
	 * @param userId	用户id		
	 * @param orgId		组织机构id
	 * @return 
	 * List<UserDetail>
	 * <br/><b>创 建 人：</b>wangzelu
	 * <br/><b>创建时间:</b>2019年7月19日 下午1:40:02
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年7月19日 下午1:40:02
	 * @since  1.0.0
	 */
	public List<UserDetail> getUserListByParams(Map<String, List<String>> keywordMap, String userId, String orgId);
	
	/**
	 * 方法描述: 通过用户（不一定挂在组织机构的哪个层级）取售电公司的组织机构代码
	 * 				通过system_config表中的配置获取“售电公司”的特征leveltype，然后根据用户名挂靠的组织机构向父级组织查询，一直查到符合leveltype的组织机构
	 * @param userId：登录用户id；level：级别；type：类型
	 * @return 
	 * OrgDetail
	 * <br/><b>创 建 人：</b>wangzelu
	 * <br/><b>创建时间:</b>2019年7月19日 下午7:18:22
	 * <br/><b>修 改 人：</b>wangzelu
	 * <br/><b>修改时间:</b>2019年7月19日 下午7:18:22
	 * @since  1.0.0
	 */
	public OrgDetail getCompanyOrgByUserId(String userId,String level,String type);
	
	
	/**-------------------------------------代维客户用户接口--------------------------------**/

	/**
	 * 方法描述: 根据当前登录人获取ac的组织机构(只有省，市，公司)
	 * @return
	 * List<TreeNode>
	 * <br/><b>创 建 人：</b>zhangshuwei
	 * <br/><b>创建时间:</b>2019年8月26日 下午14:23:22
	 * <br/><b>修 改 人：</b>zhangshuwei
	 * <br/><b>修改时间:</b>2019年8月26日 下午14:23:22
	 * @since  1.0.0
	 */
	public List<TreeNode> getAcOrgTree(String userId);


	
	/**
	 * @Title: getBigUserListByOrg
	 * @Description: 通过单位ID获取该单位/部门下及子单位下大集体用户的Id集合
	 * @return
	 * Map<String,Object>
	 * <b>创 建 人：</b>zhaohonghao<br/>
	 * <b>创建时间:</b>2019年8月29日16:31:52
	 * <b>修 改 人：</b>zhh<br/>
	 * <b>修改时间:</b>2019年8月29日16:32:00
	 * @since  1.0.0
	 */
	public List<Map<String, Object>> getBigUserListByOrg(Map<String, Object> param);
	
	/**
	 * @Title: getAcConsumerByUserId
	 * @Description: 通过用户ID获取该该用户信息和该用户所属客户信息
	 * @return
	 * Map<String,Object>
	 * <b>创 建 人：</b>zhaohonghao<br/>
	 * <b>创建时间:</b>2019年8月30日14:17:29
	 * <b>修 改 人：</b>zhaohonghao<br/>
	 * <b>修改时间:</b>2019年8月30日14:17:32
	 * @since  1.0.0
	 */
	public Map<String, Object> getAcConsumerByUserId(String id);


	
	/*************************************代维业务班组所用接口*************************/

	/**
	 * @Title: getUserDetailExcludingTeam
	 * @Description: 查询该单位下代维中没有被分组的人员  page rows list<String>
	 * @return
	 * Map<String,Object>
	 * <b>创 建 人：</b>zsw<br/>
	 * <b>创建时间:</b>2019年8月28日 下午16:13:22
	 * <b>修 改 人：</b>zsw<br/>
	 * <b>修改时间:</b>2019年8月28日 下午16:13:22
	 * @since  1.0.0
	 */
	public Map<String, Object> getUserDetailExcludingTeam(Map<String, Object> map);

```





