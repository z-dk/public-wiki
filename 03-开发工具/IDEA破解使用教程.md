### 1. 下载idea，并安装
```
http://192.168.14.6/static/public/idea/ideaIU-2018.3.2.exe
```

#### 2. 下载一个 JetbrainsIdesCrack-3.4-release-enc.jar 破解补丁。放在你的安装idea下面的bin的目录下面。
下载链接：

```
http://192.168.14.6/static/public/idea/JetbrainsIdesCrack-3.4-release-enc.jar
```


#### 3. 在安装的idea下面的bin目录下面有2个文件 ： 一个是idea64.exe.vmoptions，还有一个是idea.exe.vmoptions
#### 4. 用记事本打开（蓝色框） 分别在最下面一行增加一行

```
-javaagent:F:\idea\IntelliJ IDEA 2017.1\bin\JetbrainsIdesCrack-3.4-release-enc.jar
```

注意：“F:\idea\IntelliJ IDEA 2017.1\bin\JetbrainsIdesCrack-3.4-release-enc.jar” 是对应的JetbrainsIdesCrack-3.4-release-enc.jar的位置。
重启一下软件，在进入出现有active code选择界面的时候 写入下面注册代码

```
K71U8DBPNE-eyJsaWNlbnNlSWQiOiJLNzFVOERCUE5FIiwibGljZW5zZWVOYW1lIjoibGFuIHl1IiwiYXNzaWduZWVOYW1lIjoiIiwiYXNzaWduZWVFbWFpbCI6IiIsImxpY2Vuc2VSZXN0cmljdGlvbiI6IkZvciBlZHVjYXRpb25hbCB1c2Ugb25seSIsImNoZWNrQ29uY3VycmVudFVzZSI6ZmFsc2UsInByb2R1Y3RzIjpbeyJjb2RlIjoiSUkiLCJwYWlkVXBUbyI6IjIwMTktMDUtMDQifSx7ImNvZGUiOiJSUzAiLCJwYWlkVXBUbyI6IjIwMTktMDUtMDQifSx7ImNvZGUiOiJXUyIsInBhaWRVcFRvIjoiMjAxOS0wNS0wNCJ9LHsiY29kZSI6IlJEIiwicGFpZFVwVG8iOiIyMDE5LTA1LTA0In0seyJjb2RlIjoiUkMiLCJwYWlkVXBUbyI6IjIwMTktMDUtMDQifSx7ImNvZGUiOiJEQyIsInBhaWRVcFRvIjoiMjAxOS0wNS0wNCJ9LHsiY29kZSI6IkRCIiwicGFpZFVwVG8iOiIyMDE5LTA1LTA0In0seyJjb2RlIjoiUk0iLCJwYWlkVXBUbyI6IjIwMTktMDUtMDQifSx7ImNvZGUiOiJETSIsInBhaWRVcFRvIjoiMjAxOS0wNS0wNCJ9LHsiY29kZSI6IkFDIiwicGFpZFVwVG8iOiIyMDE5LTA1LTA0In0seyJjb2RlIjoiRFBOIiwicGFpZFVwVG8iOiIyMDE5LTA1LTA0In0seyJjb2RlIjoiR08iLCJwYWlkVXBUbyI6IjIwMTktMDUtMDQifSx7ImNvZGUiOiJQUyIsInBhaWRVcFRvIjoiMjAxOS0wNS0wNCJ9LHsiY29kZSI6IkNMIiwicGFpZFVwVG8iOiIyMDE5LTA1LTA0In0seyJjb2RlIjoiUEMiLCJwYWlkVXBUbyI6IjIwMTktMDUtMDQifSx7ImNvZGUiOiJSU1UiLCJwYWlkVXBUbyI6IjIwMTktMDUtMDQifV0sImhhc2giOiI4OTA4Mjg5LzAiLCJncmFjZVBlcmlvZERheXMiOjAsImF1dG9Qcm9sb25nYXRlZCI6ZmFsc2UsImlzQXV0b1Byb2xvbmdhdGVkIjpmYWxzZX0=-Owt3/+LdCpedvF0eQ8635yYt0+ZLtCfIHOKzSrx5hBtbKGYRPFDrdgQAK6lJjexl2emLBcUq729K1+ukY9Js0nx1NH09l9Rw4c7k9wUksLl6RWx7Hcdcma1AHolfSp79NynSMZzQQLFohNyjD+dXfXM5GYd2OTHya0zYjTNMmAJuuRsapJMP9F1z7UTpMpLMxS/JaCWdyX6qIs+funJdPF7bjzYAQBvtbz+6SANBgN36gG1B2xHhccTn6WE8vagwwSNuM70egpahcTktoHxI7uS1JGN9gKAr6nbp+8DbFz3a2wd+XoF3nSJb/d2f/6zJR8yJF8AOyb30kwg3zf5cWw==-MIIEPjCCAiagAwIBAgIBBTANBgkqhkiG9w0BAQsFADAYMRYwFAYDVQQDDA1KZXRQcm9maWxlIENBMB4XDTE1MTEwMjA4MjE0OFoXDTE4MTEwMTA4MjE0OFowETEPMA0GA1UEAwwGcHJvZDN5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxcQkq+zdxlR2mmRYBPzGbUNdMN6OaXiXzxIWtMEkrJMO/5oUfQJbLLuMSMK0QHFmaI37WShyxZcfRCidwXjot4zmNBKnlyHodDij/78TmVqFl8nOeD5+07B8VEaIu7c3E1N+e1doC6wht4I4+IEmtsPAdoaj5WCQVQbrI8KeT8M9VcBIWX7fD0fhexfg3ZRt0xqwMcXGNp3DdJHiO0rCdU+Itv7EmtnSVq9jBG1usMSFvMowR25mju2JcPFp1+I4ZI+FqgR8gyG8oiNDyNEoAbsR3lOpI7grUYSvkB/xVy/VoklPCK2h0f0GJxFjnye8NT1PAywoyl7RmiAVRE/EKwIDAQABo4GZMIGWMAkGA1UdEwQCMAAwHQYDVR0OBBYEFGEpG9oZGcfLMGNBkY7SgHiMGgTcMEgGA1UdIwRBMD+AFKOetkhnQhI2Qb1t4Lm0oFKLl/GzoRykGjAYMRYwFAYDVQQDDA1KZXRQcm9maWxlIENBggkA0myxg7KDeeEwEwYDVR0lBAwwCgYIKwYBBQUHAwEwCwYDVR0PBAQDAgWgMA0GCSqGSIb3DQEBCwUAA4ICAQC9WZuYgQedSuOc5TOUSrRigMw4/+wuC5EtZBfvdl4HT/8vzMW/oUlIP4YCvA0XKyBaCJ2iX+ZCDKoPfiYXiaSiH+HxAPV6J79vvouxKrWg2XV6ShFtPLP+0gPdGq3x9R3+kJbmAm8w+FOdlWqAfJrLvpzMGNeDU14YGXiZ9bVzmIQbwrBA+c/F4tlK/DV07dsNExihqFoibnqDiVNTGombaU2dDup2gwKdL81ua8EIcGNExHe82kjF4zwfadHk3bQVvbfdAwxcDy4xBjs3L4raPLU3yenSzr/OEur1+jfOxnQSmEcMXKXgrAQ9U55gwjcOFKrgOxEdek/Sk1VfOjvS+nuM4eyEruFMfaZHzoQiuw4IqgGc45ohFH0UUyjYcuFxxDSU9lMCv8qdHKm+wnPRb0l9l5vXsCBDuhAGYD6ss+Ga+aDY6f/qXZuUCEUOH3QUNbbCUlviSz6+GiRnt1kA9N2Qachl+2yBfaqUqr8h7Z2gsx5LcIf5kYNsqJ0GavXTVyWh7PYiKX4bs354ZQLUwwa/cG++2+wNWP+HtBhVxMRNTdVhSm38AknZlD+PTAsWGu9GyLmhti2EnVwGybSD2Dxmhxk3IPCkhKAK+pl0eWYGZWG3tJ9mZ7SowcXLWDFAk0lRJnKGFMTggrWjV8GYpw5bq23VmIqqDLgkNzuoog==

```

5. 如果上边注册码过期，从下面网址获取验证码
```
http://idea.lanyus.com/
```

6. 使用前请将下面地址添加到hosts文件中
```
0.0.0.0 account.jetbrains.com
```
修改host教程

```
https://jingyan.baidu.com/article/4b52d7027130d4fc5c774b2c.html
```

#### SVN配置

##### 安装客户端
第一步

![design image](https://img2018.cnblogs.com/blog/1120344/201811/1120344-20181120170722691-1995111588.png)

第二步

![design image](https://img2018.cnblogs.com/blog/1120344/201811/1120344-20181120170733265-1983500035.png)

第三步

![design image](https://img2018.cnblogs.com/blog/1120344/201811/1120344-20181120170910330-1665772100.png)

第四步，这一步必须选择安装命令行

![design image](https://img2018.cnblogs.com/blog/1120344/201811/1120344-20181120171142095-1883823733.png)

第五步

![design image](https://img2018.cnblogs.com/blog/1120344/201811/1120344-20181120171254078-977280262.png)

第六步

![design image](https://img2018.cnblogs.com/blog/1120344/201811/1120344-20181120171513998-1538816942.png)

##### 配置SVN

进入file -> settings -> version control -> subversion , 命令行svn.exe 的路径确定正确即可，

##### svn使用  底部 version control 

* local changes 本地修改的文件，新添加的 文件如果没有添加到版本管理中则在Unversioned Files中显示，这个提交的时候要过滤和提交，
* Repository svn提交记录等
* incoming 还未同步的记录，可以点击进行更新，如果有不同行的代码冲突会自动合并，如果出现同行代码冲突则会提示，需要手动选择线上版本、本地版本、或解决冲突。
* Subversion Working Copies information svn版本库信息。


#### maven 配置

##### 单项目配置

进入file -> settings -> maven 或项目右侧maven 的配置， 配置对应的maven 安装目录和对应的setting.xml 

##### 默认项目配置(必须配置，否则下次打开新项目还需要再次配置)

进入file -> other settings -> settings for new objects -> maven 或项目右侧maven 的配置， 配置对应的maven 安装目录和对应的setting.xml 

#### 配置tomcat

选择运行方式

![design image](https://images2017.cnblogs.com/blog/1196371/201707/1196371-20170726154514500-1639196721.png)

添加新的运行方式，选择tomcat

![design image](https://images2017.cnblogs.com/blog/1196371/201707/1196371-20170726154610734-2086163676.png)

配置tomcat位置，vm option配置内存大小，选中before launch 中的想，点击减号全部删除掉， 勾选Deploy applications configured in Tomcat instance 使用本地配置,

![design image](https://images2017.cnblogs.com/blog/1196371/201707/1196371-20170726154733687-1299224203.png)

#### 快捷键

* 搜索类、接口等 ctrl+alt+n
* 全文检索 Ctrl+Shift+F
* 全文检索 Ctrl+Shift+R
* 自动辅助 alt+回车
* 自动生成get set 构造函数等 alt+insert

#### 窗口区域

*   左上 项目
*   左下 结构
*   右上 maven

#### 多项目

* 一次只打开一个项目
* 多个项目打开多个窗口
* 打开多个项目时点击new window
* 可以打开最近的项目