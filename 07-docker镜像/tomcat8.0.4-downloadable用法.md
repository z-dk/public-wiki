#### 最新版本tomcat 镜像支持以下功能

* 单点集群直接使用配置切换，不用再映射配置文件
* 直接指定 war文件的URL 即可自动下载部署，集群环境只需要配置成一个war地址即可
* 字体文件已经打入镜像内部，工作流不会出现乱码
* 时间时区文件已经打入镜像，不会出现容器内时区不一致导致时间错误
* war 文件不存在自动停止运行容器

#### 下载地址

```

http://ysd.hhwy.org/static/wars/tomcat8.0.28-jre1708-downloadable.tar

linux 可以使用 curl 下载  命令为 curl http://ysd.hhwy.org/static/wars/tomcat8.0.28-jre1708-downloadable.tar
linux 可以使用 wget 下载  命令为 wget http://ysd.hhwy.org/static/wars/tomcat8.0.28-jre1708-downloadable.tar

```

#### 导入镜像命令

```
docker load < tomcat8.0.28-jre1708-downloadable.tar

```

#### yml文件环境变量配置说明

* cluster_mode ：是否开启集群模式  取值范围[open,close], 默认为关闭
* session_redis_hosts ： 集群session 共享的redis 地址  例如：192.168.1.74:26380,192.168.1.75:26380,192.168.1.764:26380
* session_redis_master ：session共享如果是sentinel 模式需要提供 master标识，  ，如果不设置则认为是redis 单点模式。例如： session_redis_master: master01
* download_war ： 要运行的war的下载地址， tomcat启动之前会自动下载该war  例如：download_war: http://192.168.1.74:81/static/wars\monitor-platform-web.war

#### yml 文件案例

```
monitor-platform:
    image: "tomcat8.0.28-jre1708-downloadable"
    expose: 
        - 8080
    ports:
        - "9091:8080"
    environment:
        JAVA_OPTS: "-server -Xms4096M -Xmx4096M -Xmn1024m -Xss512k -XX:+AggressiveOpts -XX:+UseBiasedLocking -XX:PermSize=1536M -XX:MaxPermSize=1536M -XX:+DisableExplicitGC -XX:MaxTenuringThreshold=20 -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:+UseParNewGC  -XX:+CMSParallelRemarkEnabled -XX:+UseCMSCompactAtFullCollection -XX:LargePageSizeInBytes=128m  -XX:+UseFastAccessorMethods -XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=30"
        ds_url: "jdbc:mysql://10.0.2.15:3306/monitor_platform?useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true"
        ds_userName: "monitor"
        ds_password: "monitor@hhwy"
        ds.changedatabase : "false"
        common.domain: "default"
        fweb.frontserver.hosts: 10.0.2.16
        fweb.frontserver.port: 6001
        fweb.frontserver.platform: "monitor-platform"
        consmanagement.add.type: "monitor"
        fweb.rpc.test.invoke: ""
        common.zookeeper.hosts: "10.0.2.16:2181"
        fweb.cached.hosts: "10.0.2.16:6379"
        fweb.cached.dbnum: "2"
        fweb.import.initdata: "true"
        fweb.cached.default.type: "com.hhwy.fweb.cached.redis.RedisCacheClientPool"
        download_war: http://ysd.hhwy.org/static/wars/monitor-platform-web.war
        
```

#### 启动与停止

```
docker-compose up -d 启动

docker-compose down 停止


```

####  运行日志说明

运行后在前边会显示出 运行日志，包括下载状态，是否下载成功等。 日志内容如下


``` 
 download war = http://192.168.1.74:81/static/wars\monitor-platform-web.war  #配置的war地址
 file_name=monitor-platform-web.war #文件名
 replaced=http://192.168.1.74:81/static/wars/monitor-platform-web.war # 替换过的war地址，把\ 全部替换为 /
 downloading ... ... ... # 下载中
 downloaded # 下载完成
 total 124M # webapps  下执行ls -l
 drwxr-xr-x.  3 root root 4.0K Dec  8 16:47 ROOT
 drwxr-xr-x. 14 root root 4.0K Mar  1  2017 docs
 drwxr-xr-x.  6 root root 4.0K Mar  1  2017 examples
 drwxr-xr-x.  5 root root 4.0K Mar  1  2017 host-manager
 drwxr-xr-x.  5 root root 4.0K Mar  1  2017 manager
 -rw-r--r--.  1 root root 124M Dec  9 20:00 monitor-platform-web.war # 可以看到目录中有次文件
 cluster mode=open # 集群模式
 cluster open , mv /usr/local/tomcat/confbak/context.xml /usr/local/tomcat/conf/context.xml  #使用集群模式的配置
 session store redis address=192.168.1.74:26380,192.168.1.75:26380,192.168.1.764:26380 #session 共享的redis 地址
 master=master01 sentinen 模式的 master标识

```