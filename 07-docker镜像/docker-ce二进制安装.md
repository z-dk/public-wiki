## 从二进制文件安装Docker CE

## 先决条件
* A 64-bit installation
* Version 3.10 or higher of the Linux kernel. The latest version of the kernel available for your platform is recommended.
* iptables version 1.4 or higher
* git version 1.7 or higher
* A ps executable, usually provided by procps or a similar package.
* XZ Utils 4.9 or higher
* A properly mounted cgroupfs hierarchy; a single, all-encompassing cgroup mount point is not sufficient. See Github issues #2683, #3485, #4568).

## 安装

### 下载二进制文件

```
wget  https://download.docker.com/linux/static/stable/x86_64/docker-18.09.5.tgz
```
### 解压

```
 tar xzvf docker-18.09.5.tgz
```

### 移动到/usr/local/docker
```
 mv  ./docker  /usr/local/docker
```

### 配置环境变量
```
  vi /etc/profile 添加如下信息
  PATH=$PATH:/usr/local/docker
  export PATH
```

### 存储驱动目前使用overlay2, 其他模式请参考官方文档

### 需要更新系统内核为4.1 以上版本

```
下载源码
wget https://mirrors.edge.kernel.org/pub/linux/kernel/v4.x/linux-4.14.103.tar.gz
```

```
编译安装
tar -zxf linux-4.14.103.tar.gz
cd linux-4.14.103
make mrproper
make clean              #这两条用于清除之前编译生成的.ko和.config
make oldconfig          #使用当前的内核配置
make                    #编译，半小时以上是正常的
make modules_install    #把编译出的内核模块复制到/lib/modules/${KERNEL_VERSION}
make install
```
* make oldconfig行表示使用当前内核一样的配置，坏消息是一般新的功能还是要我们自己选择而且项数还不少，好消息是这些选择都有默认选项如果我们不在意这些新功能看也不看一直按住回车即可。
* make config是每项都要自己选择，这个除了自己明白要什么配置不然不建议使用。
* make default是直接使用linus提供的默认配置，这个基本什么不用选编译也很快但是也不建议用，编译快是因为很多东西都没启用使用这个内核你的发行版基本重启启不来了。

### yum 方式升级内核

```
rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-3.el7.elrepo.noarch.rpm
yum --enablerepo=elrepo-kernel install kernel-ml-devel kernel-ml -y
```

```
修改启动文件
vi /etc/default/grub
修改
GRUB_DEFAULT=0
保存退出
grub2-mkconfig -o /boot/grub2/grub.cfg
重启
查看内核版本
uname -r
```


#### 配置日志驱动、docker数据目录、存储驱动

```
vi /etc/docker/daemon.json
{
  "log-driver": "json-file",
  "log-opts": {
            "max-size": "500m",
            "max-file": "10"
  },
  "graph": "/data/docker",
  "storage-driver": "overlay2",
  "storage-opts": [
    "overlay2.override_kernel_check=true"
  ]
}
```