## JMC 监控 docker 启动的 TOMCAT


作者：贾立新
链接：https://git.365power.cn:8443/jialx/documents
來源：简书
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

#### 概述

jdk1.8 推出了JMC：Java Mission Control JMC打开性能日志后，主要包括7部分性能报告，分别是一般信息、内存、代码、线程、I/O、系统、事件。其中，内存、代码、线程及I/O是系统分析的主要部分，本文会重点进行阐述。由于 jmc 需要开启商用模式， 目前官方提供的tomcat镜像全部采用open jdk ，不能开启。所以需要制作  tomcat 镜像 

#### jdk 1.7 镜像制作 Dockerfile

```
FROM centos

ENV LANG C.UTF-8
ENV JAVA_DEBIAN_VERSION=1.7.0_80
ENV JAVA_VERSION=1.7.0_80
ENV JAVA_HOME=/usr/lib/jvm/java-1.7.0_80/jre
ENV PATH $JAVA_HOME"/bin:$PATH

RUN mkdir -p "$JAVA_HOME"
#把jre 复制到镜像中
COPY jre "$JAVA_HOME" 
RUN chmod 755 "$JAVA_HOME"/bin/*



```

#### 制作 tomcat 8.0.47 镜像


首先在宿主机上编译出 apr 需要的文件

##### 下载并解压
http://http://mirrors.aliyun.com/apache/apr/apr-1.5.2.tar.gz
##### 下载并解压
http://mirrors.aliyun.com/apache/apr/apr-util-1.5.4.tar.gz


##### 安装apr

```
cd /usr/local/src
tar -zxf apr-1.5.2.tar.gz
cd apr-1.5.2
./configure --prefix=/usr/local/apr
make
make install

```

##### 安装apr-util

```
cd /usr/local/src
tar -zxf apr-util-1.5.4.tar.gz
cd apr-util-1.5.4
./configure --with-apr=/usr/local/apr
make
make install

```

##### 安装tomcat native, 这个文件在tomcat bin文件夹中会存在

```
tar -zxf tomcat-native.tar.gz
cd tomcat-native-1.2.8-src/native/
./configure --with-apr=/usr/local/apr 
make
make install

```


到 /usr/local/apr 复制出整个文件夹， apr/lib下的文件内容如下

```
apr.exp 
libapr-1.a
libapr-1.so    
libapr-1.so.0.5.2  
libaprutil-1.la  
libaprutil-1.so.0      
libexpat.a   
libexpat.so    
libexpat.so.0.5.0  
libtcnative-1.la  
libtcnative-1.so.0       
pkgconfig
aprutil.exp  
libapr-1.la  
libapr-1.so.0  
libaprutil-1.a     
libaprutil-1.so  
libaprutil-1.so.0.5.4  
libexpat.la  
libexpat.so.0  
libtcnative-1.a    
libtcnative-1.so  
libtcnative-1.so.0.2.16

```

##### 编写Dockerfile

```
FROM jdk1.7-centos

ENV APR_HOME /usr/local/apr
ENV CATALINA_HOME /usr/local/tomcat
ENV PATH $CATALINA_HOME/bin:$PATH
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$APR_HOME/lib
ENV TOMCAT_MAJOR 8
ENV TOMCAT_VERSION 8.0.47
# 设置工作目录
WORKDIR $CATALINA_HOME
# 创建文件夹
RUN mkdir -p $CATALINA_HOME
# 创建文件夹
RUN mkdir -p $APR_HOME
# 复制编译好的apr文件到镜像中
COPY apr $APR_HOME
# 下载的tomcat ,删除掉没用的bat, example 文件等,复制到镜像中
COPY tomcat $CATALINA_HOME
# 赋权限
RUN chmod 755 $CATALINA_HOME/bin/*
# 映射端口
EXPOSE 8080
CMD ["catalina.sh", "run"]

```



