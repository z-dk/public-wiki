#### 安装pip

```
   yum install python-pip

```

#### 如果没有源

```
   yum -y install epel-release
   
   执行成功之后，再次执行yum install python-pip

```

#### 升级pip

```
    pip install --upgrade pip

```

#### 安装docker-compose

```
pip install docker-compose

```
#### 运行docker-compose


#### 如果出现报错

```
   pkg_resources.DistributionNotFound: backports.ssl-match-hostname>=3.5
   使用pip 更新backports.ssl-match-hostname的版本
   pip install --upgrade backports.ssl_match_hostname
   更新backports.ssl_match_hostname 到3.5版本后问题解决
```