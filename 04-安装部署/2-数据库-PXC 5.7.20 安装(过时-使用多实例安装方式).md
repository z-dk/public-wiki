### PXC 简介

##### 特性

* (1).同步复制 Synchronous replication
* (2).Active-active multi-master 拓扑逻辑
* (3).可对集群中任一节点进行数据读写
* (4).自动成员控制，故障节点自动从集群中移除
* (5).自动节点加入
* (6).真正并行的复制，基于行级
* (7).直接客户端连接，原生的 MySQL 接口
* (8).每个节点都包含完整的数据副本
* (9).多台数据库中数据同步由 wsrep 接口实现

##### 局限性

* (0).多点写入性能下降，死锁和事务冲突增多，目前为单点写入，读写分离
* (1).目前的复制仅仅支持InnoDB存储引擎,任何写入其他引擎的表，包括mysql.*表将不会复制,但是DDL语句会被复制的,因此创建用户将会被复制,但是insert into mysql.user…将不会被复制的.
* (2).DELETE操作不支持没有主键的表,没有主键的表在不同的节点顺序将不同,如果执行SELECT…LIMIT… 将出现不同的结果集.
* (3).在多主环境下LOCK/UNLOCK TABLES不支持,以及锁函数GET_LOCK(), RELEASE_LOCK()…
* (4).查询日志不能保存在表中。如果开启查询日志，只能保存到文件中。
* (5).允许最大的事务大小由wsrep_max_ws_rows和wsrep_max_ws_size定义。任何大型操作将被拒绝。如大型的LOAD DATA操作。
* (6).由于集群是乐观的并发控制，事务commit可能在该阶段中止。如果有两个事务向在集群中不同的节点向同一行写入并提交，失败的节点将中止。对 于集群级别的中止，集群返回死锁错误代码(Error: 1213 SQLSTATE: 40001 (ER_LOCK_DEADLOCK)).
* (7).XA事务不支持，由于在提交上可能回滚。
* (8).整个集群的写入吞吐量是由最弱的节点限制，如果有一个节点变得缓慢，那么整个集群将是缓慢的。为了稳定的高性能要求，所有的节点应使用统一的硬件。
* (9).集群节点建议最少3个。
* (10).如果DDL语句有问题将破坏集群。

##### 工作原理

Galera Cluster/ PXC 集群工作原理

client端向server端发送dml更新操作请求时，server的native本地进程处理请求，并返回OK准备接收，client发送commit更新事务给server，server将replicate writeset复制写数据集发给group（cluster集群），cluster将该数据集对应产生的唯一的GTID（global transaction ID）发送给集群每个server（节点）。当前server节点验证通过后，执行commit_cd动作更新本地数据库，并返回OK；若其他节点验证不通过，则执行rollback_cd，回滚刚提交的事务。其他server(other server)接收并验证通过后，执行apply_cd和commit_cd动作更新本地数据库；若验证不通过，则丢弃该数据集。

#### 名词解释

* mariadb
* galera cluster 
* PXC  Percona XtraDB Cluster简称PXC。Percona Xtradb Cluster的实现是在原mysql代码上通过Galera包将不同的mysql实例连接起来，实现了multi-master的集群架构  ,pxc 可以认为是 mysql Galera Cluster
* SST   State Snapshot Transfer 全量同步，命令service mysql start --wsrep_sst_donor=mariadb-02 ，   应该对新加节点先使用备份恢复之后做IST ,可以减少很多资源的使用，查找相关资料
* IST   Incremental State Transfer 增量同步
* 4个端口号 3306 数据库对外服务的端口号 ,4444 请求SST SST: 指数据一个镜象传输 xtrabackup , rsync ,mysqldump 4567 : 组成员之间进行沟通的一个端口号 4568 : 传输IST用的。相对于SST来说的一个增量。



### 官方文档地址

```
原版安装文档
https://www.percona.com/doc/percona-xtradb-cluster/5.7/configure.html

各个版本
https://www.percona.com/doc/percona-xtradb-cluster/5.5/howtos/centos_howto.html

5.7介绍，以及各种方式安装percona-server(并不是PXC,只是作为参考)
https://www.percona.com/doc/percona-server/5.7/index.html
5.7 yum and RPM 安装percona-server(并不是PXC,只是作为参考)
https://www.percona.com/doc/percona-server/5.7/installation/yum_repo.html#standalone-rpm

下载Percona-Server-5.7，选择对应版本(并不是PXC)
https://www.percona.com/downloads/Percona-Server-5.7/
其他资源下载
https://www.percona.com/downloads/

```



### 环境介绍

```
三个节点 10.0.2.47  48  49
CPU 16核
内存 32G

```

### 首先安装 yum 源 （RPM  yum 安装都需要）

``` shell
 rpm -Uhv https://www.percona.com/redir/downloads/percona-release/redhat/0.1-6/percona-release-0.1-6.noarch.rpm
```
#### 或创建以下yum 源文件 percona-release.repo
```
########################################
# Percona releases and sources, stable #
########################################
[percona-release-$basearch]
name = Percona-Release YUM repository - $basearch
baseurl = http://repo.percona.com/release/$releasever/RPMS/$basearch
enabled = 1
gpgcheck = 1
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-Percona

[percona-release-noarch]
name = Percona-Release YUM repository - noarch
baseurl = http://repo.percona.com/release/$releasever/RPMS/noarch
enabled = 1
gpgcheck = 1
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-Percona 

[percona-release-source]
name = Percona-Release YUM repository - Source packages
baseurl = http://repo.percona.com/release/$releasever/SRPMS
enabled = 0
gpgcheck = 1
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-Percona 

####################################################################
# Testing & pre-release packages. You don't need it for production #
####################################################################
[percona-testing-$basearch]
name = Percona-Testing YUM repository - $basearch
baseurl = http://repo.percona.com/testing/$releasever/RPMS/$basearch
enabled = 0
gpgcheck = 1
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-Percona

[percona-testing-noarch]
name = Percona-Testing YUM repository - noarch
baseurl = http://repo.percona.com/testing/$releasever/RPMS/noarch
enabled = 0
gpgcheck = 1
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-Percona

[percona-testing-source]
name = Percona-Testing YUM repository - Source packages
baseurl = http://repo.percona.com/testing/$releasever/SRPMS
enabled = 0
gpgcheck = 1
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-Percona

############################################
# Experimental packages, use with caution! #
############################################
[percona-experimental-$basearch]
name = Percona-Experimental YUM repository - $basearch
baseurl = http://repo.percona.com/experimental/$releasever/RPMS/$basearch
enabled = 0
gpgcheck = 1
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-Percona

[percona-experimental-noarch]
name = Percona-Experimental YUM repository - noarch
baseurl = http://repo.percona.com/experimental/$releasever/RPMS/noarch
enabled = 0
gpgcheck = 1
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-Percona

[percona-experimental-source]
name = Percona-Experimental YUM repository - Source packages
baseurl = http://repo.percona.com/experimental/$releasever/SRPMS
enabled = 0
gpgcheck = 1
gpgkey = file:///etc/pki/rpm-gpg/RPM-GPG-KEY-Percona

```

### 以下分为yum 安装 和 RPM 包安装方式，任选其一，不要都安装。并且版本不一样，yum 为5.7.23   RPM 为5.7.20，按需选择

### PXC yum安装5.7.23


#### 验证是否有对应版本，必须有5.7.23 版本

``` shell
yum list Percona-XtraDB-Cluster-server-*

```

#### 安装(所有节点)

``` shell
yum install Percona-XtraDB-Cluster-server-57.x86_64 Percona-XtraDB-Cluster-client-57.x86_64 Percona-XtraDB-Cluster-galera-3.x86_64
```

### PXC rpm 安装 5.7.20

#### 下载相关 bundle.tar

``` shell
wget https://www.percona.com/downloads/Percona-XtraDB-Cluster-57/Percona-XtraDB-Cluster-5.7.20-29.24/binary/redhat/7/x86_64/Percona-XtraDB-Cluster-5.7.20-29.24-r386-el7-x86_64-bundle.tar
```
#### 解压

``` shell
tar xvf Percona-XtraDB-Cluster-5.7.20-29.24-r386-el7-x86_64-bundle.tar
```
#### 查看文件个数
``` shell
ll *.rpm 

-rw-rw-r-- 1 root root     27488 Jan 25  2018 Percona-XtraDB-Cluster-57-5.7.20-29.24.1.el7.x86_64.rpm
-rw-rw-r-- 1 root root 104585452 Jan 25  2018 Percona-XtraDB-Cluster-57-debuginfo-5.7.20-29.24.1.el7.x86_64.rpm
-rw-rw-r-- 1 root root   7484008 Jan 25  2018 Percona-XtraDB-Cluster-client-57-5.7.20-29.24.1.el7.x86_64.rpm
-rw-rw-r-- 1 root root   1123840 Jan 25  2018 Percona-XtraDB-Cluster-devel-57-5.7.20-29.24.1.el7.x86_64.rpm
-rw-rw-r-- 1 root root     26988 Jan 25  2018 Percona-XtraDB-Cluster-full-57-5.7.20-29.24.1.el7.x86_64.rpm
-rw-rw-r-- 1 root root    705336 Jan 25  2018 Percona-XtraDB-Cluster-garbd-57-5.7.20-29.24.1.el7.x86_64.rpm
-rw-rw-r-- 1 root root  52789996 Jan 25  2018 Percona-XtraDB-Cluster-server-57-5.7.20-29.24.1.el7.x86_64.rpm
-rw-rw-r-- 1 root root    752808 Jan 25  2018 Percona-XtraDB-Cluster-shared-57-5.7.20-29.24.1.el7.x86_64.rpm
-rw-rw-r-- 1 root root   1184164 Jan 25  2018 Percona-XtraDB-Cluster-shared-compat-57-5.7.20-29.24.1.el7.x86_64.rpm
-rw-rw-r-- 1 root root  28356064 Jan 25  2018 Percona-XtraDB-Cluster-test-57-5.7.20-29.24.1.el7.x86_64.rpm

```

#### 安装

``` shell
yum install *.rpm

通过yum 安装可以解决部分依赖问题。否则需要安装很多包，安装过程中如果出现galera 的东西冲突，需要先卸载galera的rpm
```


### 配置

#### 创建对应目录和文件

``` shell
mkdir -p /data/mysql/logs
mkdir -p /data/mysql/binlogs
mkdir -p /data/mysql/datas
mkdir -p /data/mysql/share
mkdir -p /data/mysql/etc
touch /data/mysql/share/errmsg.sys
赋给mysql用户
chown -R mysql:mysql /data/mysql
```

#### 配置时区

```
timedatectl set-timezone Asia/Shanghai
rm -rf /etc/localtime
cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
```

#### 开启对应端口(所有节点)
```
firewall-cmd --zone=public --add-port=3306/tcp --permanent 
firewall-cmd --zone=public --add-port=4567/tcp --permanent 
firewall-cmd --zone=public --add-port=4568/tcp --permanent 
firewall-cmd --zone=public --add-port=4444/tcp --permanent 
firewall-cmd --reload

```

#### mysqld 配置文件 /data/mysql/etc/my.cnf（所有节点）

```
[client]
default-character-set=utf8
socket=/data/mysql/mysql.sock
[mysqld]


bind-address=0.0.0.0
user=mysql
port=3306

basedir=/data/mysql
datadir=/data/mysql/datas #?¾?¿¼
pid-file=/data/mysql/mysqld.pid
log-bin=/data/mysql/binlogs/bin-log
log-error=/data/mysql/logs/mysqld.log
slow_query_log_file=/data/mysql/logs/slow_query.log  #½«²é·µ»ؽ?ýä?¼?¼
socket=/data/mysql/mysql.sock
character-set-filesystem = utf8


innodb_autoinc_lock_mode=2
slow_query_log=1
binlog_format=ROW
binlog_cache_size = 2M
max_binlog_size=512M
long_query_time =2  #?ִָ?³¬¹ý?ql»áog?4£¬???2?
log-queries-not-using-indexes=0
back_log=500
sql_mode=STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
lower_case_table_names=1
transaction_isolation=READ-COMMITTED

default_storage_engine=innodb
innodb_autoinc_lock_mode=2
innodb_flush_method=O_DIRECT
innodb_buffer_pool_size=10G
innodb_buffer_pool_instances=10
innodb_max_dirty_pages_pct=90
innodb_log_buffer_size=12M
innodb_log_file_size =512M
innodb_log_files_in_group=4
innodb_autoextend_increment=128
innodb_flush_log_at_trx_commit=2
innodb_open_files=1500


# connection limit has been reached.
max_connections=2000
max_connect_errors=1200
open_files_limit=65535
table_open_cache = 600
table_definition_cache=600
performance_schema_max_table_instances=1024
tmp_table_size = 256M
thread_cache_size = 256


# Set the query cache.
query_cache_size = 1G
query_cache_type = ON
query_cache_limit= 4M

max_heap_table_size= 256M
max_allowed_packet = 32M
max_heap_table_size = 8M

# buffer size
key_buffer_size = 1024M
read_buffer_size = 4M
sort_buffer_size = 4M
join_buffer_size = 4M
read_rnd_buffer_size=4M
sync_binlog=15

[mysql_safe]
log-error=/data/mysql/logs/mysqld.log

```
### wsrep配置文件 /data/mysql/etc/wsrep.cnf（所有节点）

```
#wsrep
server-id=1 #1-254
wsrep_provider=/usr/lib64/galera3/libgalera_smm.so
wsrep_provider_options="gcache.size=1G; gcache.page_size=300M"
wsrep_cluster_name="gip_galera_cluster"
wsrep_cluster_address="gcomm://10.0.2.47,10.0.2.48,10.0.2.49"  #gcomm://ip1,ip2,ip3
wsrep_node_name=pxc-01
wsrep_node_address="10.0.2.47"
#wsrep sst
wsrep_sst_method=xtrabackup-v2
wsrep_sst_auth=sstuser:Hhwy@sst2018

```

#### 第二个节点/data/mysql/etc/wsrep.cnf

```
server-id=2 #1-254
wsrep_node_name=pxc-02 #当前节点的名称，这个应该可以根据需要起名
wsrep_node_address="10.0.2.48" #当前节点的ip地址
``` 
#### 第三个节点/data/mysql/etc/wsrep.cnf

```
server-id=3 #1-254
wsrep_node_name=pxc-03 #当前节点的名称，这个应该可以根据需要起名
wsrep_node_address="10.0.2.49" #当前节点的ip地址
``` 

#### /etc/my.cnf 文件引入

``` 
!include /data/mysql/etc/my.cnf
!include /data/mysql/etc/wsrep.cnf
```



### PXC 启动

#### 第一个节点(C1)

``` shell
 systemctl start mysql@bootstrap.service
```

#### 设置root 密码 以及 SST 账号

#### 由于root密码为随机生成需要到日志中找到
```
cat /data/mysql/logs/mysqld.log |grep password
```

``` shell
首先登陆mysql
mysql -h127.0.0.1 -uroot -pOcT:DuaIE8_w
```

```
#必须先修改密码
alter user user() identified by 'Hhwy@admin2018';  
GRANT ALL PRIVILEGES ON *.* TO 'admin'@'%' IDENTIFIED BY 'Hhwy@admin2018' WITH GRANT OPTION; 
GRANT ALL PRIVILEGES ON *.* TO 'admin'@'localhost' IDENTIFIED BY 'Hhwy@admin2018' WITH GRANT OPTION; 
GRANT ALL PRIVILEGES ON *.* TO 'sstuser'@'%' IDENTIFIED BY 'Hhwy@sst2018' WITH GRANT OPTION; 
GRANT ALL PRIVILEGES ON *.* TO 'sstuser'@'localhost' IDENTIFIED BY 'Hhwy@sst2018' WITH GRANT OPTION; 
flush privileges;

```


#### 启动其他节点(C2)
``` shell 
systemctl start mysql
```

### PXC 新增节点

```
安装pxc ,之后配置好后，直接启动就可以了，等待SST完成后就启动完成了，如果数据较多SST时间较长
```

### 查看状态

```
show status like "wsrep%"
```


### 重启
* 在停止前确保断绝没有应用在使用节点，否则可能造成事务未提交。之后依次停止mysql 
* 按照最后停机的最先起原则启动，第一个启动的参考(C1),其他节点参考(C2)
* 如果不是人为停机则参考故障恢复



### 故障恢复


* mysqld_safe --wsrep-recover   #查看每个节点的序列号,执行后如下信息


```
2018-09-21 14:30:29 139805244745856 [Note] InnoDB: Starting final batch to recover 31 pages from redo log.
2018-09-21 14:30:30 139805244745856 [Note] InnoDB: Last binlog file '/data/mysql/log_bin/mysql-bin.000142', position 51044687
2018-09-21 14:31:20 139805244745856 [Note] InnoDB: 128 out of 128 rollback segments are active.
2018-09-21 14:31:20 139805244745856 [Note] InnoDB: Creating shared tablespace for temporary tables
2018-09-21 14:31:20 139805244745856 [Note] InnoDB: Setting file './ibtmp1' size to 12 MB. Physically writing the file full; Please wait ...
2018-09-21 14:31:20 139805244745856 [Note] InnoDB: File './ibtmp1' size is now 12 MB.
2018-09-21 14:31:20 139805244745856 [Note] InnoDB: Waiting for purge to start
2018-09-21 14:31:20 139805244745856 [Note] InnoDB: 5.7.23 started; log sequence number 212477063280
2018-09-21 14:31:20 139805244745856 [Warning] InnoDB: Skipping buffer pool dump/restore during wsrep recovery.
2018-09-21 14:31:20 139805244745856 [Note] Plugin 'FEEDBACK' is disabled.
2018-09-21 14:31:20 139805244745856 [Note] Recovering after a crash using /data/mysql/log_bin/mysql-bin
2018-09-21 14:31:20 139805244745856 [Note] Starting crash recovery...
2018-09-21 14:31:20 139805244745856 [Note] Crash recovery finished.
2018-09-21 14:31:20 139805244745856 [Note] Server socket created on IP: '::'.
2018-09-21 14:31:20 139805244745856 [Note] WSREP: Recovered position: 29b95562-b9b8-11e8-bf35-22431512e694:416205

```

* 查看  /data/mysql/grastate.dat , 根据安装mysql 的目录不同文件位置不同

如下信息，将每个节点的 29b95562-b9b8-11e8-bf35-22431512e694 分别写道自己的grastate.dat 的uuid 中，冒号后边416205 写道 seqno 中， 将集群中seqno序列号最高的safe_to_bootstrap 写为1，

```
#GALERA saved state
version: 2.1
uuid:    00000000-0000-0000-0000-000000000000
seqno:   -1
safe_to_bootstrap: 0

```
* 讲safe_to_bootstrap为1的作为第一个启动的节点。恢复之前最好备份数据，以免造成不可恢复的错误。
* 第一个启动节点参考(C1) ，其他节点参考(C2)   ,如果没问题应该都可以启动成功
* 如果恢复失败应该启动SST 做全量恢复，根据数据量大小时间应该会比较长

#### SST 如果机器出现无法恢复的问题或者新加入节点需要执行SST 

*  service mysql start --wsrep_sst_donor=nodeC 使用此命令保证nodeC 在hosts 中能找到对应IP, nodeC为sst 源节点，在sst的过程中此节点会比较慢，最好可以让其停止服务
*  如果在SST 执行时时间比较长需要修改过期时间否则会报出如下错误  timed out
* 可以修改timeout.conf  文件增加过期时间，参考上边
* 实操过程中发现修改后还是报time out  可以尝试修改服务文件的过期时间，最后是用 mysqld_safe --defaults-file=/etc/my.cnf --user=mysql --wsrep_sst_donor=mariadb-02  启动的
* SST 有多种方式，如果使用rsync 出现gcs connect failed: Address already in use  ，需要删除 mysql 目录下的rsync.pid
```
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10418]: sent 82 bytes  received 1611006236 bytes  total size 1610612736
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10491]: connect from mariadb-02 (10.0.2.48)
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10492]: connect from mariadb-02 (10.0.2.48)
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10493]: connect from mariadb-02 (10.0.2.48)
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10494]: connect from mariadb-02 (10.0.2.48)
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10496]: connect from mariadb-02 (10.0.2.48)
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10495]: connect from mariadb-02 (10.0.2.48)
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10492]: rsync to rsync_sst/./zabbix from mariadb-02 (10.0.2.48)
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10494]: rsync to rsync_sst/./mysql from mariadb-02 (10.0.2.48)
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10493]: rsync to rsync_sst/./log_bin from mariadb-02 (10.0.2.48)
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10491]: rsync to rsync_sst/./epmp from mariadb-02 (10.0.2.48)
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10495]: rsync to rsync_sst/./performance_schema from mariadb-02 (10.0.2.48)
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10496]: rsync to rsync_sst/./test from mariadb-02 (10.0.2.48)
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10494]: receiving file list
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10492]: receiving file list
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10495]: receiving file list
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10493]: receiving file list
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10496]: receiving file list
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10491]: receiving file list
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10496]: sent 86 bytes  received 100380 bytes  total size 100080
Sep 21 17:52:26 sxyxcs-mariadb3 rsyncd[10495]: sent 48 bytes  received 222 bytes  total size 61
Sep 21 17:52:30 sxyxcs-mariadb3 rsyncd[10494]: sent 1682 bytes  received 109896706 bytes  total size 109863814
Sep 21 17:52:32 sxyxcs-mariadb3 rsyncd[10492]: sent 5368 bytes  received 179678064 bytes  total size 179614546
Sep 21 17:52:43 sxyxcs-mariadb3 rsyncd[10493]: sent 637 bytes  received 948393733 bytes  total size 948160313
Sep 21 17:53:39 sxyxcs-mariadb3 systemd[1]: mariadb.service start operation timed out. Terminating.
Sep 21 17:53:39 sxyxcs-mariadb3 mysqld[10341]: Terminated
Sep 21 17:53:39 sxyxcs-mariadb3 mysqld[10341]: WSREP_SST: [INFO] Joiner cleanup. rsync PID: 10387 (20180921 17:53:39.596)
Sep 21 17:53:39 sxyxcs-mariadb3 rsyncd[10387]: sent 0 bytes  received 0 bytes  total size 0
Sep 21 17:53:40 sxyxcs-mariadb3 mysqld[10341]: WSREP_SST: [INFO] Joiner cleanup done. (20180921 17:53:40.103)
Sep 21 17:54:01 sxyxcs-mariadb3 rsyncd[10491]: rsync error: received SIGINT, SIGTERM, or SIGHUP (code 20) at io.c(504) [generator=3.1.2]
Sep 21 17:54:01 sxyxcs-mariadb3 rsyncd[10491]: rsync error: received SIGINT, SIGTERM, or SIGHUP (code 20) at io.c(504) [receiver=3.1.2]
Sep 21 17:55:09 sxyxcs-mariadb3 systemd[1]: mariadb.service stop-final-sigterm timed out. Skipping SIGKILL. Entering failed mode.
Sep 21 17:55:09 sxyxcs-mariadb3 systemd[1]: Failed to start MariaDB 10.2.17 database server.

```

#### 案例一  三个节点，关闭一个

   由于维护和配置变更等工作需要，正常关闭节点A，其它节点会收到”good by”信息，cluster size大小会减少，一些属性如仲裁计算和自动增长都会自动改变。一旦我们再次启动节点A，它将会基于my.cnf文件中的wsrep_cluster_address设置加入集群.这个过程是很不同于正常复制---加入者节点不会提供任何请求，直到再次与集群完全同步，所以正连接到集群中提供同等作用的节点是不足的，首次ST必需成功。如果在节点A关闭期间，节点B或节点C的写数据集gcache中仍然存在他们所有执行过的事务，加入集群可能通过增量同步(IST，快和轻量)，否则，需要进行全量同步，实际上就是全量二进制数据复制。因此，决定一个最佳贡献者是重要的，如果因贡献者gcache丢失了事务，增量同步是不可能发生，则回退决定由贡献者和全量同步自动代替

如图
![image](https://img-blog.csdn.net/20160709223516776?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQv/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/Center)

#### 案例二  三个节点，关闭两个

  节点A和节点B正常关闭。通过熟悉前面的案例，cluster size减少到1。因此，即使是单个节点C，组成一个主要组件，提供客户端请求。获得这些节点要返回到集群中的需求时，你仅需要启动他们，可是，当它必需提供ST给首个加入的节点时，节点C切换到“Donor/Desynced”状态。在那个过程期间，它仍然是可以读写的，但是它可能非常慢，取决于它发送的st有多大。所以，一些负载均衡器可能认为贡献者节点是不可操作的或是从池中移除了。所以，最好避免当只有一个节点在线时的场景。

需要注意的是，如果你按顺序重启节点A和节点B，当节点A的gcache可能不含有所有需要的写数据集时，你可以想要节点B确保不使用节点A作为ST贡献者。所以指定节点C作为贡献者的方法(你指定一个带有wsrep_node_name变量的节点名为node C):

* service mysql start --wsrep_sst_donor=nodeC 
* 确保nodeC 在hosts 中配置

#### 案例三:三个节点，全部关闭 

   所有三个节点都正常关闭。集群已废，在这个案例中，这问题是如何再次初始化，这里有一点很重要的要知道，在节点干净关闭期间，PXC节点会将最后一个执行位置写进grastate.dat文件中，通过比较文件里面的seqno号，你会看到最先进位置的节点(最可能是最后关闭的节点)，集群必需使用这个节点自举启动，否则，先进位置的节点必需与后进位置节点执行全量同步初始化加入集群(和一些事务将会丢失) 

* 给出一些自举启动第一个节点的脚本：
```
/etc/init.d/mysql bootstrap-pxc或 service mysql bootstrap-pxc或service mysql start --wsrep_new_cluster或service mysql start --wsrep-cluster-address="gcomm://"

或在centos 7中使用系统服务包：systemctl start mysql@bootstrap.service，在老的pxc版本中，自举启动集群，需要在my.cnf文件中将wsrep_cluster_address变量的内容替换为空：wsrep_cluster_address=gcomm:// 。
```
 

#### 案例四：一个节点从集群中消失

   节点A从集群中消失。消失的意思是指断电、硬件损坏、内核错误，mysql崩溃、kill -9 mysqld进程等，两个剩余的节点注意到连接A节点已关闭，并尝试重新连接到它。一些超时后，两个节点都同意节点A确实关闭和从集群中正式移除它。仲裁保留（三个中的两个正常在线），因此，没有中断服务发生。重启后，节点A按场景一的相同的方法自动加入集群 

#### 案例五：两个节点从集群中消失 

 节点A和节点B消失，节点C不能组成单独仲裁，所以集群切换进入非主模式，mysql拒绝提供任何SQL查询，在这个状态中，节点C的mysql进程仍然运行，你能连接到它，但任何语句相关的数据失败都带有： 
```
mysql> select * from test.t1;
ERROR 1047 (08S01): Unknown command 
```
实际上节点C在一瞬间读是可能的，直到它决定不能到达节点A和节点B，但没有立即新写入的被允许,由于galera的基于复制的认证。

我们将看到剩余节点的日志是什么： 
```
140814 0:42:13 [Note] WSREP: commit failed for reason: 3
140814 0:42:13 [Note] WSREP: conflict state: 0
140814 0:42:13 [Note] WSREP: cluster conflict due to certification failure for threads:
140814 0:42:13 [Note] WSREP: Victim thread:
THD: 7, mode: local, state: executing, conflict: cert failure, seqno: -1
SQL: insert into t values (1)  
```
单个节点C正等待它的同等作用的伙伴们再次在线，在一些案例中，当网络中断和在所有时间那些节点up时，集群会再自动组成。如果节点B和节点C与第一节点的网络断了，但它们之间仍相互到达，当他们组成仲裁时，他们能保持自己的功能，如果节点A和B崩溃(数据不一致、bug等)或者由于断电下线，你需要人工在节点C上启用主组件，你能将节点A和B带回集群中，这个方法，我们可以告诉节点C,你可以单独组成一个集群，可以忘记节点A和节点B，这个命令是 
```
SET GLOBAL wsrep_provider_options='pc.bootstrap=true';  
```
可是，你在做这个操作前，需要多检查确认其它节点是否真正down了，最有可能最终会有两个不同的数据集群。 

#### 案例六 所有节点不正确地关闭
这个场景可以发生在数据中心电源失败，命中mysql或galera bug以致于所有节点崩溃的案例中，但数据一致性结果要作出让步—--集群检测到每个节点的数据不同，在那些案例的每一个里，grastate.dat 文件不能被更新和不能包含有效的seqno号，它可能看起来像这个： 
```
#GALERA saved state
version: 2.1
uuid: 220dcdcb-1629-11e4-add3-aec059ad3734
seqno: -1
safe_to_bootstrap: 0
```
在这个案例中，我们不能确信所有节点之间是否一致的，所以关键是要找到最前进节点，用它来自举启动集群，在任何节点上启动mysql daemon前，你必须通过检查它的事务状态来提取最后的序列号，你能按下面方法来做

```
[root@percona3 ~]# mysqld_safe  --defaults-file=/etc/my.cnf --user=mysql --wsrep-recover 
140821 15:57:15 mysqld_safe Logging to '/var/lib/mysql/percona3_error.log'.
140821 15:57:15 mysqld_safe Starting mysqld daemon with databases from /var/lib/mysql
140821 15:57:15 mysqld_safe WSREP: Running position recovery with --log_error='/var/lib/mysql/wsrep_recovery.6bUIqM' --pid-file='/var/lib/mysql/percona3-recover.pid'
140821 15:57:17 mysqld_safe WSREP: Recovered position 4b83bbe6-28bb-11e4-a885-4fc539d5eb6a:2
140821 15:57:19 mysqld_safe mysqld from pid file /var/lib/mysql/percona3.pid ended 
```

所以在这个节点上的最后提交的事务序列号是2，现在你需要先从最后节点上自举启动，然后再启动其它节点。 然而，以上方法在新的galera版本3.6及以上是不需要的，自pxc 5.6.19是可用的。

有一个新的选项--- pc.recovery(默认是启用)，它用于保留集群状态到每个节点上的gvwstate.dat文件里，当作变量名说明(pc – primary component)，它只保留一个主要状态的集群。

那个文件的内容看起来像如下：
```
cat /var/lib/mysql/gvwstate.dat
my_uuid: 76de8ad9-2aac-11e4-8089-d27fd06893b9
#vwbeg
view_id: 3 6c821ecc-2aac-11e4-85a5-56fe513c651f 3
bootstrap: 0
member: 6c821ecc-2aac-11e4-85a5-56fe513c651f 0
member: 6d80ec1b-2aac-11e4-8d1e-b2b2f6caf018 0
member: 76de8ad9-2aac-11e4-8089-d27fd06893b9 0
#vwend
```
 我们能看到以上三个节点集群是启动的，多亏了这项新功能，在我们的数据中心停电的情况下，上电后回来，节点将读取启动最后的状态，将尝试恢复主组件一旦所有成员再次开始看到对方。这使得PXC集群自动被关闭而无需任何人工干预恢复。在log中，我们可以看到：
```
140823 15:28:55 [Note] WSREP: restore pc from disk successfully
(...)
140823 15:29:59 [Note] WSREP: declaring 6c821ecc at tcp://192.168.90.3:4567 stable
140823 15:29:59 [Note] WSREP: declaring 6d80ec1b at tcp://192.168.90.4:4567 stable
140823 15:29:59 [Warning] WSREP: no nodes coming from prim view, prim not possible
140823 15:29:59 [Note] WSREP: New COMPONENT: primary = no, bootstrap = no, my_idx = 2, memb_num = 3
140823 15:29:59 [Note] WSREP: Flow-control interval: [28, 28]
140823 15:29:59 [Note] WSREP: Received NON-PRIMARY.
140823 15:29:59 [Note] WSREP: New cluster view: global state: 4b83bbe6-28bb-11e4-a885-4fc539d5eb6a:11, view# -1: non-Primary, number of nodes: 3, my index: 2, protocol version -1
140823 15:29:59 [Note] WSREP: wsrep_notify_cmd is not defined, skipping notification.
140823 15:29:59 [Note] WSREP: promote to primary component
140823 15:29:59 [Note] WSREP: save pc into disk
140823 15:29:59 [Note] WSREP: New COMPONENT: primary = yes, bootstrap = yes, my_idx = 2, memb_num = 3
140823 15:29:59 [Note] WSREP: STATE EXCHANGE: Waiting for state UUID.
140823 15:29:59 [Note] WSREP: clear restored view
(...)
140823 15:29:59 [Note] WSREP: Bootstrapped primary 00000000-0000-0000-0000-000000000000 found: 3.
140823 15:29:59 [Note] WSREP: Quorum results:
version = 3,
component = PRIMARY,
conf_id = -1,
members = 3/3 (joined/total),
act_id = 11,
last_appl. = -1,
protocols = 0/6/2 (gcs/repl/appl),
group UUID = 4b83bbe6-28bb-11e4-a885-4fc539d5eb6a
140823 15:29:59 [Note] WSREP: Flow-control interval: [28, 28]
140823 15:29:59 [Note] WSREP: Restored state OPEN -> JOINED (11)
140823 15:29:59 [Note] WSREP: New cluster view: global state: 4b83bbe6-28bb-11e4-a885-4fc539d5eb6a:11, view# 0: Primary, number of nodes: 3, my index: 2, protocol version 2
140823 15:29:59 [Note] WSREP: wsrep_notify_cmd is not defined, skipping notification.
140823 15:29:59 [Note] WSREP: REPL Protocols: 6 (3, 2)
140823 15:29:59 [Note] WSREP: Service thread queue flushed.
140823 15:29:59 [Note] WSREP: Assign initial position for certification: 11, protocol version: 3
140823 15:29:59 [Note] WSREP: Service thread queue flushed.
140823 15:29:59 [Note] WSREP: Member 1.0 (percona3) synced with group.
140823 15:29:59 [Note] WSREP: Member 2.0 (percona1) synced with group.
140823 15:29:59 [Note] WSREP: Shifting JOINED -> SYNCED (TO: 11)
140823 15:29:59 [Note] WSREP: Synchronized with group, ready for connections
```