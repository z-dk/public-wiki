
#### 原版文档

```
https://www.percona.com/doc/percona-xtradb-cluster/LATEST/install/tarball.html#tarball
```

#### 环境说明

    监听3306、3307 两个端口的实例

    第一个节点：#3306 for mysqld port, 4444 for sst port, 4568 for ist port, 4567 for cluster communication port 端口作用及说明见上面
    第二个节点：#3307 for mysqld port, 5555 for sst port, 5568 for ist port, 5567 for cluster communication port 端口作用及说明见上面

#### 相关依赖

##### percona 依赖
```
rpm -Uhv https://www.percona.com/redir/downloads/percona-release/redhat/0.1-6/percona-release-0.1-6.noarch.rpm
```

##### 安装gcc socat libev  openssl必须为1.0.2k
```
 yum install libev socat  libcurl-devel libaio openssl openssl-devel
```

##### xtrabackup
```
yum install percona-xtrabackup-24
```

#### 创建用户、组
```
    groupadd mysql && useradd -r -g mysql mysql
```

#### 二进制安装

##### 首先下载二进制文件,必须对应ssl101

```
wget https://www.percona.com/downloads/Percona-XtraDB-Cluster-57/Percona-XtraDB-Cluster-5.7.20-29.24/binary/tarball/Percona-XtraDB-Cluster-5.7.20-rel18-29.24.1.Linux.x86_64.ssl101.tar.gz
```

```
解压到以下路径
/usr/local/mysql/pxc5.7.20
```

#### 配置环境变量

```
vi /etc/profile
```

```
export PATH=$PATH:/usr/local/mysql/pxc5.7.20/bin
```
#### 编写启动脚本（/usr/local/mysql/pxc5.7.20/mysql.server.sh）

``` shell
#!/bin/bash
# Copyright Abandoned 1996 TCX DataKonsult AB & Monty Program KB & Detron HB
# This file is public domain and comes with NO WARRANTY of any kind

# MySQL ($version_desc) daemon start/stop script.

# Usually this is put in /etc/init.d (at least on machines SYSV R4 based
# systems) and linked to /etc/rc3.d/S99mysql and /etc/rc0.d/K01mysql.
# When this is done the mysql server will be started when the machine is
# started and shut down when the systems goes down.

##########################################################
#      variables
##########################################################
#config file path
CONFIG_FILE="";
#mysqld pid file
PID_FILE="";
#mysqld lock file 
LOCK_FILE_PATH="";
#operate
OPERATE="";
#basedir
BASEDIR="";
#datadir
DATADIR=""
#service_startup_timeout
SERVICE_STARTUP_TIMEOUT=500
#startup_sleep
STARTUP_SLEEP=1;

##########################################################
#      script body
##########################################################

# root
if [ $USER != 'root' ]; then
    echo "Please run in as the root."
    exit 1
fi
echo "**********************************************  "
OLD_IFS="$IFS"

for i in $*            #在"$*"中遍历参数，此时"$*"被扩展为包含所有位置参数的单个字符串，只遍历一次
do
    
    IFS="=" 
    tmp_arr=($i)
    IFS=$OLD_IFS;

    left=${tmp_arr[0]};
    right=${tmp_arr[1]};
    #echo "$i"
    #echo "left=$left"
    #echo "right=$right"
    if [[ "$left" == "--defaults-file" ]]; then
       CONFIG_FILE=$right;
       echo "info: CONFIG_FILE=$CONFIG_FILE"
    fi
    if [[ "$left" == "--file" ]]; then
       CONFIG_FILE=$right;
       echo "info: CONFIG_FILE=$CONFIG_FILE"
    fi
    
    if [[ "$i" == "start" ]]; then
       OPERATE=$i;
       echo "info: mode=$OPERATE"
    fi

    if [[ "$i" == "bootstrap-pxc" ]]; then
       OPERATE=$i;
       echo "info: mode=$OPERATE"
    fi

    if [[ "$i" == "stop" ]]; then
       OPERATE=$i;
       echo "info: mode=$OPERATE"
    fi

    if [[ "$i" == "status" ]]; then
       OPERATE=$i;
       echo "info: mode=$OPERATE"
    fi
done

# config is null
if [[ -z $CONFIG_FILE ]]; then
    echo "error: --defaults-file is null"
    exit 1
fi
if [[ -z $OPERATE ]]; then
    echo "error: operate is null; start,stop,status"
    exit 1
fi
# config is not exists
if [[ ! -f $CONFIG_FILE ]]; then
    echo "error: the file [$CONFIG_FILE] is not exists"
    exit 1
fi

# parse server configs
parse_server_arguments() {
for arg do
    case "$arg" in
      --basedir=*)  BASEDIR=`echo "$arg" | sed -e 's/^[^=]*=//'`
        ;;
      --datadir=*)  DATADIR=`echo "$arg" | sed -e 's/^[^=]*=//'`
        ;;
      --pid-file=*) PID_FILE=`echo "$arg" | sed -e 's/^[^=]*=//'`
      ;;
      --socket=*) LOCK_FILE_PATH=`echo "$arg" | sed -e 's/^[^=]*=//'`
                  LOCK_FILE_PATH=${LOCK_FILE_PATH}.lock
      ;;
    esac
done
}

parse_server_arguments `my_print_defaults -c $CONFIG_FILE mysqld`

if [[ -z $BASEDIR ]]; then
    echo "error: --defaults-file basedir not found"
    exit 1
fi

if [[ -z $PID_FILE ]]; then
    echo "error: --defaults-file pidfile not found;PID_FILE=$PID_FILE"
    exit 1
fi

if [[ -z $DATADIR ]]; then
    echo "error: DATADIR is null"
    exit 1
fi

echo "info: config --basedir=$BASEDIR"
echo "info: config --datadir=$DATADIR"
echo "info: config --pid-file=$PID_FILE"
echo "**********************************************  "
# wait_for_pid  
wait_for_pid () {
  verb="$1"           # created | removed
  pid="$2"            # process ID of the program operating on the pid-file
  pid_file_path="$3" # path to the PID file.
  sst_progress_file=$DATADIR/sst_in_progress
  i=0
  avoid_race_condition="by checking again"

  while test $i -ne $SERVICE_STARTUP_TIMEOUT ; do

    case "$verb" in
      'created')
        # wait for a PID-file to pop into existence.
        test -s "$pid_file_path" && i='' && break
        ;;
      'removed')
        # wait for this PID-file to disappear
        test ! -s "$pid_file_path" && i='' && break
        ;;
      *)
        echo "wait_for_pid () usage: wait_for_pid created|removed pid pid_file_path"
        exit 1
        ;;
    esac

    # if server isn't running, then pid-file will never be updated
    if test -n "$pid"; then
      if kill -0 "$pid" 2>/dev/null; then
        :  # the server still runs
      else
        # The server may have exited between the last pid-file check and now.  
        if test -n "$avoid_race_condition"; then
          avoid_race_condition=""
          continue  # Check again.
        fi

        # there's nothing that will affect the file.
        echo "The server quit without updating PID file ($pid_file_path)."
        return 1  # not waiting any more.
      fi
    fi

    if test -e $sst_progress_file && [ $STARTUP_SLEEP -ne 10 ];then
        echo "SST in progress, setting sleep higher"
        STARTUP_SLEEP=10
    fi

    echo -n $echo_n ".$echo_c"
    i=`expr $i + 1`
    sleep $STARTUP_SLEEP

  done
  echo ""
  if test -z "$i" ; then
    return 0
  elif test -e $sst_progress_file; then 
    return 2
  else
    return 1
  fi
}

# check running mysqld
check_running() {

    local show_msg=$1

    # First, check to see if pid file exists
    if test -s "$PID_FILE" ; then 
        read mysqld_pid < "$PID_FILE"
        if kill -0 $mysqld_pid 2>/dev/null ; then 
            echo "MySQL running ($mysqld_pid)"
            return 0
        else
            echo "MySQL is not running, but PID file exists"
            return 1
        fi
    else
        # Try to find appropriate mysqld process
        #mysqld_pid=`pidof $libexecdir/mysqld`
        mysqld_pid=

        # test if multiple pids exist
        pid_count=`echo $mysqld_pid | wc -w`
        if test $pid_count -gt 1 ; then
            echo "Multiple MySQL running but PID file could not be found ($mysqld_pid)"
            return 5
        elif test -z $mysqld_pid ; then 
            if test -f "$LOCK_FILE_PATH" ; then 
                echo "MySQL is not running, but lock file ($LOCK_FILE_PATH) exists"
                return 2
            fi 
            test $show_msg -eq 1 && echo "MySQL is not running"
            return 3
        else
            echo "MySQL is running but PID file could not be found"
            return 4
        fi
    fi
}



case "$OPERATE" in
        start)   
          check_running 0;

          ext_status=$?
          if test $ext_status -ne 3;then 
              exit $ext_status
          fi
          #startup mysql server
          echo "MySQL running";
          mysqld_safe --defaults-file="$CONFIG_FILE"  >/dev/null 2>&1 &
          #sleep
          wait_for_pid created "$!" "$PID_FILE"; return_value=$?
          
          if [ $return_value == 1 ];then 
             echo "MySQL server startup failed!"
          elif [ $return_value == 2 ];then
             echo "MySQL server startup failed! SST still in progress"
          fi
        ;;
        stop)   
          # Stop daemon. We use a signal here to avoid having to know the
          # root password.
          echo $echo_n "Shutting down MySQL "
          if test -s "$PID_FILE"
          then
             mysqld_pid=`cat "$PID_FILE"`

            if (kill -0 $mysqld_pid 2>/dev/null)
            then
                kill $mysqld_pid
                # mysqld should remove the pid file when it exits, so wait for it.
                wait_for_pid removed "$mysqld_pid" "$PID_FILE"; return_value=$?
                if [ $return_value != 0 ];then 
                    echo "MySQL server stop failed!"
                fi
                
            else
                echo "MySQL server process #$mysqld_pid is not running!"
                rm "$PID_FILE"
            fi

            # Delete lock for RedHat / SuSE
            if test -f "$LOCK_FILE_PATH"
            then
                rm -f "$LOCK_FILE_PATH"
            fi
            exit $return_value
          else
            echo "MySQL PID file could not be found!"
          fi
        ;;
        status)  
          check_running 1
        ;;
        'bootstrap-pxc')
          check_running 0;

          ext_status=$?
          if test $ext_status -ne 3;then 
              exit $ext_status
          fi
          echo "MySQL running";
          #startup mysql server
          mysqld_safe --defaults-file="$CONFIG_FILE" --wsrep-new-cluster >/dev/null 2>&1 &
          #sleep
          wait_for_pid created "$!" "$PID_FILE"; return_value=$?
          
          if [ $return_value == 1 ];then 
             echo "MySQL server startup failed!"
          elif [ $return_value == 2 ];then
             echo "MySQL server startup failed! SST still in progress"
          fi
      ;;
esac
```

#### 3306配置

##### 创建3306文件夹

```
    mkdir -p /data/mysql/3306/logs
    mkdir -p /data/mysql/3306/binlogs
    mkdir -p /data/mysql/3306/datas
    mkdir -p /data/mysql/3306/share
    mkdir -p /data/mysql/3306/etc
    touch /data/mysql/3306/logs/mysqld_safe.log
    touch /data/mysql/3306/logs/mysqld.log
    赋给mysql用户
    chown -R mysql:mysql /data/mysql/3306/

```

##### my.cnf 配置文件
 
```
# 3306 for mysqld port, 4444 for sst port, 4568 for ist port, 4567 for cluster communication port 
[client]
default-character-set=utf8
socket=/data/mysql/3306/mysql.sock

[mysqld]
port=3306
bind-address=0.0.0.0
user=mysql

basedir=/data/mysql/3306
datadir=/data/mysql/3306/datas 
pid-file=/data/mysql/3306/mysqld.pid
log-bin=/data/mysql/3306/binlogs/bin-log
log-error=/data/mysql/3306/logs/mysqld.log
general_log_file=/data/mysql/3306/logs/mysqld-general.log
slow_query_log_file=/data/mysql/3306/logs/slow_query.log  
socket=/data/mysql/3306/mysql.sock
character-set-filesystem = utf8
default-time-zone = '+8:00' 
log_timestamps='system'

general_log=OFF
innodb_autoinc_lock_mode=2
slow_query_log=1
binlog_format=ROW
binlog_cache_size = 1M
enforce_gtid_consistency=on
log_slave_updates=1
gtid_mode=ON
max_binlog_size=128M
long_query_time =2  
log-queries-not-using-indexes=0
back_log=500
sql_mode=STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
lower_case_table_names=1
transaction_isolation=READ-COMMITTED
event_scheduler=on 

default_storage_engine=innodb
innodb_flush_method=O_DIRECT
innodb_buffer_pool_size=2G
innodb_buffer_pool_instances=10
innodb_max_dirty_pages_pct=75
innodb_log_buffer_size=2M
innodb_log_file_size =128M
innodb_log_files_in_group=4
innodb_autoextend_increment=128
innodb_flush_log_at_trx_commit=2
innodb_open_files=1500


# connection limit has been reached.
max_connections=100
max_connect_errors=100
open_files_limit=65535
table_open_cache = 200
table_definition_cache=400
performance_schema_max_table_instances=1024
tmp_table_size = 128M
thread_cache_size = 128


# Set the query cache.
query_cache_size = 0
query_cache_type = 0
query_cache_limit= 1M

max_heap_table_size= 64M
max_allowed_packet = 8M

# buffer size
key_buffer_size = 256M
read_buffer_size = 1M
sort_buffer_size = 1M
join_buffer_size = 1M
read_rnd_buffer_size=1M
sync_binlog=15


[mysqld_safe]
log-error=/data/mysql/3306/logs/mysqld.log
malloc-lib=/usr/lib64/libjemalloc.so.1
```

##### wsrep.cnf 配置 

```
# 3306 for mysqld port, 4444 for sst port, 4568 for ist port, 4567 for cluster communication port 
[mysqld]
server-id=1
wsrep_cluster_name="test_galera_cluster"
wsrep_provider=/usr/local/mysql/pxc5.7.20/lib/libgalera_smm.so
wsrep_cluster_address='gcomm://10.0.2.36:4567,10.0.2.36:5567' #三个节点都保持一样

wsrep_node_name=pxc-01
wsrep_node_incoming_address=10.0.2.36:3306  #客户端期望连接地址
wsrep_node_address=10.0.2.36:3306  #mysqld

wsrep_sst_receive_address=10.0.2.36:4444  #SST
wsrep_provider_options = "gmcast.listen_addr=tcp://10.0.2.36:4567;ist.recv_addr=10.0.2.36:4568;gcache.size=128M; gcache.page_size=64M;"  #监听传输，IST

#wsrep sst
wsrep_sst_method=xtrabackup-v2
wsrep_sst_auth=sstuser:Hhwy@sst2018
```
##### 

##### sum.cnf 配置

```
!include /data/mysql/3306/etc/my.cnf
!include /data/mysql/3306/etc/wsrep.cnf
```

##### 启动第一个节点

```
初始化第一个节点(只需要初始化第一个)
bin/mysqld --defaults-file=/data/mysql/3306/etc/sum.cnf --initialize  --wsrep-new-cluster
```

```
脚本启动第一个节点
./mysql.server.sh --defaults-file=/data/mysql/3306/etc/sum.cnf bootstrap-pxc
```

```
查看密码(下一步登录需要)
cat /data/mysql/3306/logs/mysqld.log |grep password
```

```
 登陆节点
 mysql -u root -h 127.0.0.1 -p"h*rJ6<hK9a?R"
```
```
修改密码、创建用户
alter user user() identified by 'Hhwy@admin2018';
GRANT ALL PRIVILEGES ON *.* TO 'admin'@'%' IDENTIFIED BY 'Hhwy@admin2018' WITH GRANT OPTION; 
GRANT ALL PRIVILEGES ON *.* TO 'admin'@'localhost' IDENTIFIED BY 'Hhwy@admin2018' WITH GRANT OPTION; 
GRANT ALL PRIVILEGES ON *.* TO 'sstuser'@'%' IDENTIFIED BY 'Hhwy@sst2018' WITH GRANT OPTION; 
GRANT ALL PRIVILEGES ON *.* TO 'sstuser'@'localhost' IDENTIFIED BY 'Hhwy@sst2018' WITH GRANT OPTION; 
flush privileges;
```

##### 开启防火墙

```
firewall-cmd --zone=public --add-port=3306/tcp --permanent 
firewall-cmd --zone=public --add-port=4567/tcp --permanent 
firewall-cmd --zone=public --add-port=4568/tcp --permanent 
firewall-cmd --zone=public --add-port=4444/tcp --permanent 
firewall-cmd --reload

```


#### 3307配置

##### 创建3307文件夹

```
    mkdir -p /data/mysql/3307/logs
    mkdir -p /data/mysql/3307/binlogs
    mkdir -p /data/mysql/3307/datas
    mkdir -p /data/mysql/3307/share
    mkdir -p /data/mysql/3307/etc
    touch /data/mysql/3307/share/errmsg.sys
    touch /data/mysql/3307/logs/mysqld_safe.log
    touch /data/mysql/3307/logs/mysqld.log
    赋给mysql用户
    chown -R mysql:mysql /data/mysql/3307/

```

##### my.cnf 配置文件

  
```
#3307 for mysqld port, 5555 for sst port, 5568 for ist port, 5567 for cluster communication port 
[client]
default-character-set=utf8
socket=/data/mysql/3307/mysql.sock

[mysqld]
port=3307
bind-address=0.0.0.0
user=mysql

basedir=/data/mysql/3307
datadir=/data/mysql/3307/datas 
pid-file=/data/mysql/3307/mysqld.pid
log-bin=/data/mysql/3307/binlogs/bin-log
log-error=/data/mysql/3307/logs/mysqld.log
general_log_file=/data/mysql/3307/logs/mysqld-general.log
slow_query_log_file=/data/mysql/3307/logs/slow_query.log  
socket=/data/mysql/3307/mysql.sock
character-set-filesystem = utf8
default-time-zone = '+8:00' 
log_timestamps='system'

general_log=OFF
innodb_autoinc_lock_mode=2
slow_query_log=1
binlog_format=ROW
binlog_cache_size = 1M
log_slave_updates=1
gtid­mode=ON
max_binlog_size=128M
long_query_time =2  
log-queries-not-using-indexes=0
back_log=500
sql_mode=STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
lower_case_table_names=1
transaction_isolation=READ-COMMITTED
event_scheduler=on 

default_storage_engine=innodb
innodb_flush_method=O_DIRECT
innodb_buffer_pool_size=2G
innodb_buffer_pool_instances=10
innodb_max_dirty_pages_pct=75
innodb_log_buffer_size=2M
innodb_log_file_size =128M
innodb_log_files_in_group=4
innodb_autoextend_increment=128
innodb_flush_log_at_trx_commit=2
innodb_open_files=1500


# connection limit has been reached.
max_connections=100
max_connect_errors=100
open_files_limit=65535
table_open_cache = 200
table_definition_cache=400
performance_schema_max_table_instances=1024
tmp_table_size = 128M
thread_cache_size = 128


# Set the query cache.
query_cache_size = 0
query_cache_type = 0
query_cache_limit= 1M

max_heap_table_size= 64M
max_allowed_packet = 8M

# buffer size
key_buffer_size = 256M
read_buffer_size = 1M
sort_buffer_size = 1M
join_buffer_size = 1M
read_rnd_buffer_size=1M
sync_binlog=15


[mysqld_safe]
log-error=/data/mysql/3307/logs/mysqld.log
malloc-lib=/usr/lib64/libjemalloc.so.1
```

##### wsrep.cnf 配置 

```
#3307 for mysqld port, 5555 for sst port, 5568 for ist port, 5567 for cluster communication port
[mysqld]
server-id=1
wsrep_cluster_name="test_galera_cluster"
wsrep_provider=/usr/local/mysql/pxc5.7.20/lib/libgalera_smm.so
wsrep_cluster_address='gcomm://10.0.2.36:4567,10.0.2.36:5567' #三个节点都保持一样

wsrep_node_name=pxc-01
wsrep_node_incoming_address=10.0.2.36:3307  #客户端期望连接地址
wsrep_node_address=10.0.2.36:3307  #mysqld

wsrep_sst_receive_address=10.0.2.36:5555  #SST
wsrep_provider_options = "gmcast.listen_addr=tcp://10.0.2.36:5567;ist.recv_addr=10.0.2.36:5568;gcache.size=128M; gcache.page_size=64M;"  #监听传输，IST

#wsrep sst
wsrep_sst_method=xtrabackup-v2
wsrep_sst_auth=sstuser:Hhwy@sst2018
```
##### 

##### sum.cnf 配置

```
!include /data/mysql/3307/etc/my.cnf
!include /data/mysql/3307/etc/wsrep.cnf
```

##### 启动

```
脚本启动第二个节点需要使用start,默认会自动sst 第一个节点数据，所以启动后不需要设置密码，创建用户
./mysql.server.sh --defaults-file=/data/mysql/3306/etc/sum.cnf start
```

##### 开启防火墙

```
firewall-cmd --zone=public --add-port=3307/tcp --permanent 
firewall-cmd --zone=public --add-port=5567/tcp --permanent 
firewall-cmd --zone=public --add-port=5568/tcp --permanent 
firewall-cmd --zone=public --add-port=5555/tcp --permanent 
firewall-cmd --reload

```