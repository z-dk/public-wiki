PDF预览功能的使用



#### 使用文件服务器的ID

* 第一个参数:文件的ID
* 第二个参数:打开页签的名称，可以使用文件名

``` javascript

MainFrameUtil.pdfPreviewById("c114170f189841e5abbf892ccef496e3","测试PDF");

```

#### 使用文件的URL

* 第一个参数:文件的URL
* 第二个参数:打开页签的名称，可以使用文件名

``` javascript

MainFrameUtil.pdfPreview("http://ysd.hhwy.org/static/pdfs/test.pdf","测试PDF");

```