# summer-vue常用组件介绍

1. 输入框组件 su-textbox：[http://static.365power.cn/static/summer-vue/v2.0.1/example/textbox.html](http://static.365power.cn/static/summer-vue/v2.0.1/example/textbox.html)

2. 下拉框组件 su-select：[http://static.365power.cn/static/summer-vue/v2.0.1/example/select.html](http://static.365power.cn/static/summer-vue/v2.0.1/example/select.html)

3. 日期组件 su-datepicker：[http://static.365power.cn/static/summer-vue/v2.0.1/example/datepicker.html](http://static.365power.cn/static/summer-vue/v2.0.1/example/datepicker.html)

4. 按钮组件 su-button：[http://static.365power.cn/static/summer-vue/v2.0.1/example/button.html](http://static.365power.cn/static/summer-vue/v2.0.1/example/button.html)

5. 表单验证组件 su-form：[http://static.365power.cn/static/summer-vue/v2.0.1/example/form-validate.html](http://static.365power.cn/static/summer-vue/v2.0.1/example/form-validate.html)

6. 复选框组件 su-checkbox：[http://static.365power.cn/static/summer-vue/v2.0.1/example/checkbox.html](http://static.365power.cn/static/summer-vue/v2.0.1/example/checkbox.html)

7. 单选框组件 su-radio：[http://static.365power.cn/static/summer-vue/v2.0.1/example/radio.html](http://static.365power.cn/static/summer-vue/v2.0.1/example/radio.html)




