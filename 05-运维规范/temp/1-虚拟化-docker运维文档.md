
### docker 运维文档

### 编写人员

* 贾立鑫

### 用途说明

 部署Tomcat ，部署方便、充分利用服务器资源

### 安装位置
使用 yum 安装

### 监控端口

无

### 启动

```
 service docker start 
```

### 验证启动是否成功

```
 service docker status
 显示正在运行 
```

```
 docker info
 正确显示docker 信息
```

### 停止

```
 service docker stop
```

### 注意事项
