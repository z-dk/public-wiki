
### redis sentinel 运维文档

### 编写人员

* 贾立鑫

### 用途说明

 如用于一体化平台产品常用数据的高速缓存，减少与数据库请求次数，提高效率，此方案为redis 主从模式，1主2从，使用哨兵模式监控主节点，主节点宕机切换其他从节点为主

### 安装位置
* 10.3.10.1  /usr/local/redis3-master
* 10.3.10.2  /usr/local/redis3-slave
* 10.3.10.3  /usr/local/redis3-slave

### 监控端口

* 6379 redis 服务端口
* 26379 redis sentinel 服务端口 
* 以上端口必须开启防火墙，程序连接使用26379，框架根据规则自动找到读、写节点

### 启动 redis 

```
 cd 到每个节点安装目录依次执行
 ./bin/redis-server redis.conf
```
### 启动 redis sentinel 
```
 cd 到每个节点安装目录依次执行
 ./bin/redis-sentinel sentinel.conf
```
### 验证启动是否成功

```
 ps -ef|grep redis
```
```
 显示如下信息则启动成功
root      6466     1  0 3月11 ?       02:42:40 ./bin/redis-server 10.3.10.2:6379
root      6506     1  0 3月11 ?       00:53:36 ./bin/redis-sentinel *:26379 [sentinel]
root     30615 36303  0 10:53 pts/1    00:00:00 grep --color=auto redis
```

```
 ./redis-cli -h 10.3.10.1  -p 6379 info
 正确显示 redis 信息，信息 Replication 中role 为 slave 或master , 如果为slave 可以看到masterhost, 如果为master 则可以看到connected_slaves 个数 
```

### 停止

  三个节点依次执行如下命令

```
 ps -ef|grep redis 
```
```
# 显示如下信息
root      6466     1  0 3月11 ?       02:42:40 ./bin/redis-server 10.3.10.2:6379
root      6506     1  0 3月11 ?       00:53:36 ./bin/redis-sentinel *:26379 [sentinel]
root     30615 36303  0 10:53 pts/1    00:00:00 grep --color=auto redis
```
```
 kill -9 6466 6506
 杀掉进程
```

### 注意事项
